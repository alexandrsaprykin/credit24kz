<?php
/** @var $this SenderViewMark */
defined( '_JEXEC' ) or die;// No direct access
JHtml::_('bootstrap.tooltip');
JHtml::_( 'behavior.formvalidation' );
JHtml::_( 'behavior.keepalive' );
JHtml::_( 'formbehavior.chosen', 'select' );
$input = JFactory::getApplication()->input;
?>
<script type="text/javascript">
    Joomla.submitbutton = function (task) {
        if (task == 'mark.cancel' || document.formvalidator.isValid(document.id('item-form'))) {
            Joomla.submitform(task, document.getElementById('item-form'));
        } else {
            alert('<?php echo $this->escape( JText::_( 'JGLOBAL_VALIDATION_FORM_FAILED' ) ); ?>');
        }
    }
</script>
<form action="<?php echo JRoute::_( 'index.php?option=com_sender&id=' . $this->form->getValue( 'id' ) ); ?>" method="post" name="adminForm" id="item-form" class="form-validate" enctype="multipart/form-data">

    <div class="form-horizontal">
		<?php echo JHtml::_( 'bootstrap.startTabSet', 'myTab', array( 'active' => 'general' ) ); ?>

		<?php echo JHtml::_( 'bootstrap.addTab', 'myTab', 'general', 'Пометка' ); ?>
        <div class="row-fluid">
            <div class="span9">
                <fieldset class="adminform">
					<?php echo $this->form->getInput( 'name' ); ?>

                    <?php echo $this->form->getInput( 'color' ); ?>
                </fieldset>
            </div>
            <div class="span3">
				<?php echo JLayoutHelper::render( 'joomla.edit.global', $this ); ?>
                <div class="form-vertical"></div>
            </div>
        </div>
		<?php echo JHtml::_( 'bootstrap.endTab' ); ?>

		<?php echo JHtml::_( 'bootstrap.endTabSet' ); ?>
    </div>

    <input type="hidden" name="task" value="" />
    <input type="hidden" name="return" value="<?php echo $input->getCmd( 'return' ); ?>" />
	<?php echo JHtml::_( 'form.token' ); ?>
</form>