<?php

// No direct access
defined('_JEXEC') or die;

/**
 * Модель редактирования текущего элемента
 * @author Sapaleks
 */
class SenderModelRequest extends JModelAdmin
{

	/**
	 * загрузка текущей формы
	 * @param Array $data
	 * @param Boolean $loadData
	 * @return Object form data
	 */
	public function getForm($data = array(), $loadData = true)
	{
		$form = $this->loadForm('com_sender.request', 'request', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		$user = JFactory::getUser();
		if (!$user->authorise('core.edit.state', '#__requests.' . $this->getState('extdataedit.id'))) {
			$form->setFieldAttribute('published', 'disabled', 'true');
			$form->setFieldAttribute('published', 'filter', 'unset');
		}
		return $form;
	}


	/**
	 * @param string $type
	 * @param string $prefix
	 * @param array $config
	 * @return JTable|mixed
	 */
	public function getTable($type = 'requests', $prefix = 'Table', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Загрузка данных в форму
	 * @return Object
	 */
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState('com_sender.edit.request.data', array());
		if (empty($data)) {
			$data = $this->getItem();
		}

		return $data;
	}

	/**
	 * Запрет удаления записи
	 * @param object $record
	 * @return bool
	 */
	protected function canDelete($record)
	{
		if (!empty($record->id)) {
			return JFactory::getUser()->authorise('core.delete', '#__requests.' . (int)$record->id);
		}
	}

	public function getAdmitad($pk = null)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true)->select('ad.*, c.name')
			->from('#__admitad_results AS ad')
			->join(
				'LEFT',
				'#__admitad_campaigns AS c ON ad.campaign_id = c.id'
			)->where('ad.request_id = ' . $this->getState('request.id'));

		$db->setQuery($query);
		$admitad = $db->loadObjectList('campaign_id');

		return $admitad;
	}

	public function getBanks($pk = null)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true)->select('br.*, b.alias, u.name as worker_name')
			->from('#__bank_requests AS br')
			->join(
				'LEFT',
				'#__banks AS b ON br.bank_id = b.id'
			)->join(
				'LEFT',
				'#__users AS u ON br.worker_id = u.id'
			)->where('br.request_id = ' . $this->getState('request.id'))
			->order('b.alias');

		$db->setQuery($query);
		$banks = $db->loadObjectList('bank_id');

		return $banks;
	}

	public function getMarks($pk = null)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true)->select('m.*')
			->from('#__request_marks AS rm')
			->join(
				'LEFT',
				'#__marks AS m ON m.id = rm.mark_id'
			)->where('rm.request_id = ' . $this->getState('request.id'));

		$db->setQuery($query);
		$marks = $db->loadObjectList();

		return $marks;
	}

	public function sendToBank($request_id, $bank_id)
	{
		$db = $this->getDbo();
		$user = JFactory::getUser();

		$worker_id = $user->id;
		$query = $this->getDbo()->getQuery(true);
		$query->update('#__bank_requests')
			->set(['sended=1', 'worker_id=' . $worker_id, 'sending_time=' . time()])
			->where(array('request_id = ' . $request_id, 'bank_id = ' . $bank_id));

		$db->setQuery($query);

		try {
			$db->execute();
		} catch (RuntimeException $e) {
			$this->setError($e->getMessage());
			return false;
		}

		return true;
	}

	/**
	 * Запрет изменения состояния
	 * @param object $record
	 * @return bool
	 */
	protected function canEditState($record)
	{
		$user = JFactory::getUser();

		// Check for existing article.
		if (!empty($record->id)) {
			return $user->authorise('core.edit.state', '#__requests.' . (int)$record->id);
		} // New article, so check against the category.
		elseif (!empty($record->catid)) {
			return $user->authorise('core.edit.state', '#__requests.' . (int)$record->catid);
		} // Default to component settings if neither article nor category known.
		else {
			return parent::canEditState('com_sender');
		}
	}

}