<!-- Оставить Отзыв -->
<div class="modal" tabindex="-1" role="dialog" id="sms-confirm" style="display: block; top: 30%; bottom: 30%;" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title">Пожалуйста, введите код, полученный в смс!</h4>
            </div>
            <div class="modal-body">
                <form class="sms_form">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" value="<?php echo $res->responseid[0]?>" data-name="id" class="for-ajax-sms">
                            <input type="hidden" value="<?php echo $_POST['contact_name']?>" data-name="name" class="for-ajax-sms">
                            <label for="code" class="error" style="color: red; display: none;  margin-left: 100px;">Введен неверный код!</label>
                            <input type="text" class="for-ajax-sms" id="code" name="code" data-name="code" required placeholder="Код из СМС" style=" display: block; margin: 0 auto; width: 400px; text-align: center;  margin-bottom: 15px;">
                        </div>

                        <div class="col-md-12">
                            <button type="submit" class="submit">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->