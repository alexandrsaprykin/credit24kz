$(document).ready(function () {
    $(".accordeon dd").hide().prev().click(function () {
        $(this).parents(".accordeon").find("dd").not(this).slideUp().prev();
        $(this).next().not(":visible").slideDown().prev();
    });
    $('dl').on('click', function () {
        if ($(this).hasClass('active') == true) {
            $('dl').removeClass('active');
            $(this).find('.fa-angle-up').attr('class', 'fa fa-3x fa-angle-down');
        } else {
            $('dl').removeClass('active');
            $(this).addClass('active');
            $('.fa-angle-up').attr('class', 'fa fa-3x fa-angle-down');
            $(this).find('.fa-angle-down').attr('class', 'fa fa-3x fa-angle-up');
        }
    });
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').focus()
    });
    $('.showAll').on('click', function (e) {
        e.preventDefault();
        $('.faq_hidden').slideDown();
        $(this).hide();
    });
    $('.rule_show').on('click', function (e) {
        e.preventDefault();
        $('.rules_hidden').slideDown();
        $(this).hide();
    });
    $('.partners_banks').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: true,
        responsive: [{
            breakpoint: 1024,
            settings: {slidesToShow: 3, slidesToScroll: 3, infinite: true}
        }, {breakpoint: 600, settings: {slidesToShow: 2, slidesToScroll: 2}}, {
            breakpoint: 480,
            settings: {slidesToShow: 1, slidesToScroll: 1}
        }]
    });
    var owl = $(".carousel");
    owl.owlCarousel({items: 4});
    $(".forward").click(function () {
        owl.trigger("owl.next");
    });
    $(".back").click(function () {
        owl.trigger("owl.prev");
    });
    $('.burger-menu').on('click', function () {
        $('.header_info').slideToggle(700);
    });
    $(window).resize(function () {
        if ($(window).width() > 992) {
            $('.header_info').removeAttr('style');
        }
        ;
    });
    $("a.topLink").click(function () {
        $("html, body").animate({scrollTop: $($(this).attr("href")).offset().top + "px"}, {
            duration: 500,
            easing: "swing"
        });
        return false;
    });
    $(".recall_form").submit(function () {
        var orderdata = {};
        $('.for-ajax-recall').each(function (index, value) {
            if ($(value)[0].tagName.toLowerCase() == 'input' && ($(value).attr('type') == 'radio' || $(value).attr('type') == 'checkbox') && $(value).is(':checked')) {
                orderdata[$(value).attr('data-name')] = $(value).val();
            } else if ($(value).attr('type') != 'radio' && $(value).attr('type') != 'checkbox') {
                orderdata[$(value).attr('data-name')] = $(value).val();
            }
        });
        $.ajax({type: "POST", url: "/index.php?option=com_sender&view=form&task=form.mail", data: orderdata}).done(function () {
            setTimeout(function () {
                $('#recall').modal('hide');
            }, 2000);
        });
        return false;
    });
    $(".second_form").submit(function () {
        var orderdata = {};
        $('.for-ajax-feedback').each(function (index, value) {
            if ($(value)[0].tagName.toLowerCase() == 'input' && ($(value).attr('type') == 'radio' || $(value).attr('type') == 'checkbox') && $(value).is(':checked')) {
                orderdata[$(value).attr('data-name')] = $(value).val();
            } else if ($(value).attr('type') != 'radio' && $(value).attr('type') != 'checkbox') {
                orderdata[$(value).attr('data-name')] = $(value).val();
            }
        });
        $.ajax({type: "POST", url: "/index.php?option=com_sender&view=form&task=form.mail", data: orderdata}).done(function () {
            setTimeout(function () {
                $('#feedback').modal('hide');
            }, 2000);
        });
        return false;
    });
    $(".question_form").submit(function () {
        var orderdata = {};
        $('.for-ajax-question').each(function (index, value) {
            if ($(value)[0].tagName.toLowerCase() == 'input' && ($(value).attr('type') == 'radio' || $(value).attr('type') == 'checkbox') && $(value).is(':checked')) {
                orderdata[$(value).attr('data-name')] = $(value).val();
            } else if ($(value).attr('type') != 'radio' && $(value).attr('type') != 'checkbox') {
                orderdata[$(value).attr('data-name')] = $(value).val();
            }
        });
        $.ajax({type: "POST", url: "/index.php?option=com_sender&view=form&task=form.mail", data: orderdata}).done(function () {
            setTimeout(function () {
                $('#question').modal('hide');
            }, 2000);
        });
        return false;
    });
    $(".sms_form").submit(function () {
        $('.sms_form .submit').attr("disabled", 'disabled');
        $('.sms_form .submit').text('Проверка...');
        $('.sms_form .error').css('display', 'none');
        var orderdata = {};
        $('.for-ajax-sms').each(function (index, value) {
            if ($(value)[0].tagName.toLowerCase() == 'input' && ($(value).attr('type') == 'radio' || $(value).attr('type') == 'checkbox') && $(value).is(':checked')) {
                orderdata[$(value).attr('data-name')] = $(value).val();
            } else if ($(value).attr('type') != 'radio' && $(value).attr('type') != 'checkbox') {
                orderdata[$(value).attr('data-name')] = $(value).val();
            }
        });

        $.ajax({type: "POST", url: "/?option=com_sender&view=form&task=form.smsConfirm", dataType: "json", data: orderdata}).done(function (data) {
            if (data['success'] == true) {
                location = '/?page=order&ok=order2';
                setTimeout(function () {
                    $('#sms-confirm').css('display', 'none');
                    $('#empty_screen').removeClass('sms-confirm');
                }, 2000);
            } else {console.log(data);
                $('.sms_form #code_sms').val('');
                $('.sms_form .submit').text('Отправить');
                $('.sms_form .submit').removeAttr("disabled");
                $('.sms_form .error').css('display', 'block');
            }


        });
        return false;
    });


});
function calc() {
    var ps1 = 8 / (100 * 12);
    var ps2 = 13 / (100 * 12);
    var sk = $('input#sum').val();
    var m = $('select#date').val();
    var ap1 = (sk * ps1) / (1 - Math.pow((1 + ps1), -m));
    var ap2 = (sk * ps2) / (1 - Math.pow((1 + ps2), -m));
    $('p.green_sum span').text(Math.round(ap1));
    $('p.red_sum span').text(Math.round(ap2));
}
$('select#date').change(function () {
    calc();
});
$(document).on('input', 'input#sum', function () {
    var value = $(this).val();
    var rep = /\D/;
    if (rep.test(value)) {
        value = value.replace(rep, '');
        $(this).val(value);
    } else {
        calc();
    }
});

$(document).on('change', '#tip', function () {
    if ($('#tip').val() == 'Я работаю официально') {
        $('#tip3').css('display', 'none').removeAttr('required');
        $('#tip2').css('display', 'block').attr('required', 'required');
    } else if ($('#tip').val() == 'Я частный предприниматель') {
        $('#tip2').css('display', 'none').removeAttr('required');
        $('#tip3').css('display', 'block').attr('required', 'required');
    } else {
        $('#tip3,#tip2').css('display', 'none').removeAttr('required');
    }

});