    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-9">
                    <div class="header_logo"><a href="/"><img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo.png" alt="logo"></a></div>
                </div>
                <div class="col-md-9 col-xs-12">
                    <div class="burger-menu">
                        <div class="burger-click-region">
                            <span class="burger-menu-piece"></span>
                            <span class="burger-menu-piece"></span>
                            <span class="burger-menu-piece"></span>
                        </div>
                    </div>
                    <div class="header_info">
                        <ul>
                            <li>Алматы, улица Толе Би, 83. Оф. 340</li>
                            <li>+77273122833</li>
                            <li>+7715372658</li>
                            <li>+7078372325</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="recall">
                        <a href="#recall" data-toggle="modal">Перезвоните мне</a>
                    </div>
                </div>
            </div>
        </div>
    </header>



