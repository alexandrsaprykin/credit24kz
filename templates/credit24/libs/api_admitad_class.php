<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 17.04.2018
 * Time: 11:00
 */

class Api_Admitad
{
    private $client_id = '57ecb9ebd0d945c61e7a74f593b7dd';
    private $client_secret = '1d26a6f281022921c1f976e2d2a002';
    private $b64_encoded = 'NTdlY2I5ZWJkMGQ5NDVjNjFlN2E3NGY1OTNiN2RkOjFkMjZhNmYyODEwMjI5MjFjMWY5NzZlMmQyYTAwMg==';
    private $refresh_token;

    protected $site_id;
    protected $token;
    protected $path;

	const campaign_ids = array(
		17398 => 'Кредит Днепр',
		14605 => 'Швидко Гроші',
		17288 => 'Cashpoint',
		18990 => 'Monobank',
		16243 => 'ПУМБ',
		16126 => 'PrivatBank',
		19417 => 'Unex',
		19059 => 'Ecredit',
	);


    public function getToken(){
        global $dbh;
        $query = $dbh->prepare(
            'SELECT token, refresh_token FROM tokens WHERE api_name = "admitad"'
        );
        $query->execute();
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $this->token = $result["token"];
        $this->refresh_token = $result["refresh_token"];
        return $result["token"];
    }

    public function updateToken($token, $refresh_token){
        global $dbh;
        $query = $dbh->prepare(
            'UPDATE tokens SET token = :token, refresh_token = :refresh_token WHERE api_name = "admitad"'
        );
        $query->bindValue(':token', $token);
        $query->bindValue(':refresh_token', $refresh_token);
        $query->execute();
		$this->token = $token;
		$this->refresh_token = $refresh_token;
        return true;
    }

    public function generateToken(){
        $data = 'grant_type=client_credentials&client_id=' . $this->client_id . '&scope=advcampaigns_for_website arecords broker_application manage_advcampaigns websites advcampaigns manage_broker_application';
        $process = curl_init('https://api.admitad.com/token/');
        //curl_setopt($process, CURLOPT_HTTPHEADER, array('application/x-www-form-urlencoded;charset=UTF-8'));
        //curl_setopt($process, CURLOPT_HEADER, 1);
        curl_setopt($process, CURLOPT_USERPWD, $this->client_id . ":" . $this->client_secret);
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_POST, 1);
        curl_setopt($process, CURLOPT_POSTFIELDS, $data);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
        $return = curl_exec($process);
		$return = json_decode($return, 1);
        curl_close($process);
        return $return;
    }

    public function generateB64Auth(){

    }

    public function refreshToken(){
        $data = 'grant_type=refresh_token&client_id=' . $this->client_id . '&refresh_token=' . $this->refresh_token . '&client_secret=' . $this->client_secret;
        $process = curl_init('https://api.admitad.com/token/');
        curl_setopt($process, CURLOPT_HTTPHEADER, array('application/x-www-form-urlencoded;charset=UTF-8'));
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_POST, 1);
        curl_setopt($process, CURLOPT_POSTFIELDS, $data);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
        $return = curl_exec($process);
        curl_close($process);
        $return = json_decode($return, 1);
        if($return["access_token"] && $return["refresh_token"]){
            $this->updateToken($return["access_token"], $return["refresh_token"]);
        }else{
			$this->logResultWriter($return);
			$res = $this->generateToken();
			$this->updateToken($res["access_token"], $res["refresh_token"]);
		}
        return $return;
    }



    public function sendGetRequest($url){
        $authorization = "Authorization: Bearer " . $this->token;
        $process = curl_init($url);
        curl_setopt($process, CURLOPT_HTTPHEADER, array('application/x-www-form-urlencoded;charset=UTF-8', $authorization));
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
        $return = curl_exec($process);
        curl_close($process);
        $return = json_decode($return, 1);
        return $return;
    }

    public function sendPostRequest($url, $post){
        $authorization = "Authorization: Bearer " . $this->token;
        $process = curl_init($url);
        curl_setopt($process, CURLOPT_HTTPHEADER, array('application/x-www-form-urlencoded;charset=UTF-8', $authorization));
        //curl_setopt($process, CURLOPT_HEADER, 1);
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_POST, 1);
        curl_setopt($process, CURLOPT_POSTFIELDS, $post);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
        $return = curl_exec($process);
        curl_close($process);
        $return = json_decode($return, 1);
        return $return;
    }

	public function logResultWriter($data)
	{
		$text = '';
		if (isset($data['errors'])) {
			foreach ($data['errors'] as $error) {
				$text = $text . (isset($error['campaign_id']) ? self::campaign_ids[$error['campaign_id']] . ': ' : '');
				if (is_array($error['message'])) {
					foreach ($error['message'] as $message) {
						$text = $text . $message[0] . ': ' . $message[1][0] . ' ';
					}
				}
				$text = $text . "  !!!  ";
			}
			$this->to_log($text);
		} elseif (isset($data['error'])) {
			$text = $data['error'] . ': ' . $data['error_description'] . "  !!!  ";
			$this->to_log($text);
		} else $this->to_log('Заявка отправлена успешно');
	}


	public function to_log($str)
	{
		//$path = '';
		//$path = '/home/credit24org/public_html/';
		$f = fopen($this->path . 'sending.log', "a+");
		fputs($f, $str . ' - ' . date("Y-m-d H:i:s") . "\r\n");
		fclose($f);

		return false;
	}

}