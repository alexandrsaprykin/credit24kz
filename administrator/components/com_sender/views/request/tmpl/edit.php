<?php
/** @var $this SenderViewRequest */
defined('_JEXEC') or die;// No direct access
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');
$input = JFactory::getApplication()->input;

JFactory::getDocument()->addScriptDeclaration('
	Joomla.submitbutton = function(task)
	{
		if (task == "request.cancel")
		{
			Joomla.submitform(task, document.getElementById("item-form"));
			// @deprecated 4.0  The following js is not needed since 3.7.0.
			if (task !== "contact.apply")
			{
				window.parent.jQuery("#itemEdit' . $this->item->id . 'Modal").modal("hide");
			}
		}
	};
');
?>
<script type="text/javascript">
    Joomla.submitbutton = function (task) {
        if (task == 'mark.cancel' || document.formvalidator.isValid(document.id('item-form'))) {
            Joomla.submitform(task, document.getElementById('item-form'));
        } else {
            alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
        }
    }

</script>
<script>
    jQuery(function ($) {
        Joomla.sendToBank = function (bank_id) {
            var request_id = <? echo $this->item->id ?>;
            $.post('index.php?option=com_sender&task=request.sendToBank&tmpl=component',
                {
                    'request_id': request_id,
                    'bank_id': bank_id
                },
                function (response, status) {
                    location.reload();
                }
            );
        };

        $('button.button-redo').click(function () {

            var ids = [<? echo $this->item->id ?>];
            $.post('/index.php?option=com_sender&view=form&task=form.sendRequestsToScript',
                {
                    'ids': ids
                },
                function (response, status) {
                    console.log(response);
                    console.log(status);
                    //if (status = 'success') {
                    location.reload();
                    //}
                }
            );
        });

        $('.search-choice-close').click(function () {

            $.post('index.php?option=com_sender&task=mark.deleteMark&tmpl=component',
                {
                    'request_id': $(this).attr('data-r-id'),
                    'mark_id': $(this).attr('data-mark-id')
                },
                function (response, status) {
                    //if (status = 'success') {
                    location.reload();
                    //}
                }
            );
        });
    });
</script>


<?php
?>
<form action="<?php echo JRoute::_('index.php?option=com_sender&id=' . $this->form->getValue('id')); ?>" method="post"
      name="adminForm" id="item-form" class="form-validate" enctype="multipart/form-data">

    <div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', 'Заявка'); ?>
        <div class="row-fluid">
            <div class="span6">
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('date'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('date'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('name'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('phone'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('phone'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('inn'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('inn'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('age'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('age'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('obl'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('obl'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('tip'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('tip'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('duration'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('duration'); ?></div>
                </div>
            </div>
            <div class="span6">
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('sum'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('sum'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('referer'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('referer'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('istok'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('istok'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('device'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('device'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('alfa_result'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('alfa_result'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('confirm_result'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('confirm_result'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('email'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('email'); ?></div>
                </div>
            </div>
        </div>
        <br>
        <div class="row-fluid">
            <div class="span6">
                <div class="control-group">
                    <div class="control-label">Метки</div>
                    <div class="controls">
                        <div class="chzn-container chzn-container-multi">
                            <ul class="chzn-choices">
								<? foreach ($this->marks as $key => $mark) : ?>
                                    <li class="search-choice"><span><?php echo $mark->name; ?></span><a
                                                class="search-choice-close"
                                                data-mark-id="<?php echo $mark->id; ?>"
                                                data-r-id="<?php echo $this->item->id; ?>"></a></li>

								<?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('notes'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('notes'); ?></div>
                </div>
            </div>
            <div class="span6">

            </div>

        </div>


		<?php echo JHtml::_('bootstrap.endTab');
		if (!empty($this->admitad)) :
			echo JHtml::_('bootstrap.addTab', 'myTab', 'banks', 'Адмитад'); ?>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="left" style="width: 20%">
						<?php echo 'Название компании'; ?>
                    </th>
                    <th style="width: 20%">
						<?php echo 'Результат'; ?>
                    </th>
                    <th>
						<?php echo 'Текст'; ?>
                    </th>
                </tr>
                </thead>
                <tbody>
				<?php
				foreach ($this->admitad as $i => $result) :
					switch ($result->result) {
						case 1:
							$class = 'icon-publish';
							$text = 'Принят';
							break;
						case 2:
							$class = 'icon-smiley-sad';
							$text = 'Отклонен';
							break;
						case 3:
							$class = 'icon-unpublish';
							$text = 'Ошибка';
							break;
						default:
							$class = 'icon-clock';
							$text = 'Данных нет';
							break;
					} ?>

                    <tr class="row<?php echo $i % 2; ?>" style="display:table-row">
                        <td class="small hidden-phone">
							<?php echo $this->escape($result->name); ?>
                        </td>
                        <td class="hidden-phone">
                            <span class="<?php echo $class; ?>" title="<?php echo $text; ?>"></span>
                        </td>
                        <td class="small hidden-phone">
							<?php echo $this->escape($result->result_text); ?>
                        </td>

                    </tr>
				<?php endforeach; ?>
                </tbody>
            </table>
			<?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php endif;
		echo JHtml::_('bootstrap.addTab', 'myTab', 'publishing', 'Банки'); ?>
        <span style="background-color: #F0F0F0; padding: 5px; margin-bottom: 5px">Текущий работник: <?php echo $this->user->name; ?> (<?php echo $this->user->id; ?>)</span>
        <table class="table table-striped">
            <thead>
            <tr>
                <th class="left" style="width: 20%">
					<?php echo 'Название банка'; ?>
                </th>
                <th style="width: 20%">
					<?php echo 'Статус'; ?>
                </th>
                <th>
					<?php echo 'Дата отправки'; ?>
                </th>
                <th>
					<?php echo 'Работник'; ?>
                </th>
                <th>
					<?php echo 'Действие'; ?>
                </th>
            </tr>
            </thead>
            <tbody>
			<?php
			foreach ($this->banks as $i => $bank) :
				switch ($bank->sended) {
					case 1:
						$class = 'icon-publish';
						$text = 'Отправлена';
						break;
					case 0:
					default:
						$class = 'icon-unpublish';
						$text = 'Не отправлена';
						break;
				} ?>

                <tr class="row<?php echo $i % 2; ?>" style="display:table-row">
                    <td class="big hidden-phone">
						<?php echo $this->escape($bank->alias); ?>
                    </td>
                    <td class="hidden-phone">
                        <span class="<?php echo $class; ?>" title="<?php echo $text; ?>"></span>
						<?php echo $text; ?>
                    </td>
                    <td class="small hidden-phone">
						<?php if ($bank->sending_time) {
							echo JHtml::_('date', $bank->sending_time, JText::_('DATE_FORMAT_LC6'));
						} else echo '-' ?>
                    </td>
                    <td class="small hidden-phone">
						<?php if ($bank->worker_id) {
							echo $bank->worker_name . ' (' . $bank->worker_id . ')';
						} else echo '-' ?>
                    </td>
                    <td class="small hidden-phone">
						<?php if ($bank->sended == 0) { ?>
                        <button onclick=" Joomla.sendToBank(<?php echo $bank->bank_id; ?>);"
                                class="btn btn-small btn-primary">
                                <span class="icon-mail" aria-hidden="true"></span>
                                Отправлено
                            </button><? } ?>
                    </td>

                </tr>
			<?php endforeach; ?>
            </tbody>
        </table>
		<?php echo JHtml::_('bootstrap.endTab'); ?>
    </div>

    <input type="hidden" name="task" value=""/>
    <input type="hidden" name="return" value="<?php echo $input->getCmd('return'); ?>"/>
	<?php echo JHtml::_('form.token'); ?>
</form>