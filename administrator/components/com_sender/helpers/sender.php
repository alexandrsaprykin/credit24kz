<?php

defined( '_JEXEC' ) or die;

/**
 * Class SenderHelper
 */
class SenderHelper
{
	/**
	 * Добавление подменю
	 * @param String $vName
	 */
	static function addSubmenu( $vName )
	{
		JHtmlSidebar::addEntry(
			JText::_( 'REQUEST_SUBMENU' ),
			'index.php?option=com_sender&view=requests',
			$vName == 'requests' );
		/*JHtmlSidebar::addEntry(
			JText::_( 'CAMPAIGN_SUBMENU' ),
			'index.php?option=com_sender&view=campaigns',
			$vName == 'campaigns' );*/
		JHtmlSidebar::addEntry(
			JText::_( 'Отметки для заявок' ),
			'index.php?option=com_sender&view=marks',
			$vName == 'marks' );
		JHtmlSidebar::addEntry(
			JText::_( 'Статистика' ),
			'index.php?option=com_sender&view=statistics',
			$vName == 'statistics' );
	}

	/**
	 * Получаем доступные действия для текущего пользователя
	 * @return JObject
	 */
	public static function getActions()
	{
		$user = JFactory::getUser();
		$result = new JObject;
		$assetName = 'com_sender';
		$actions = JAccess::getActions( $assetName );
		foreach ( $actions as $action ) {
			$result->set( $action->name, $user->authorise( $action->name, $assetName ) );
		}
		return $result;
	}
}