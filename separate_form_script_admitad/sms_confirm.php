<?php
$urlApi = 'https://alfa-credits.com.ua/sms_submit_credit/';
$data = array(
    "sms_id" => $_POST['sms_id'],
    "sms_code" => $_POST['code'],
    "id" => $_POST['id'],
);
$request = $urlApi . "?" . http_build_query($data);

/*$curl = curl_init();

if ($curl) {
    curl_setopt($curl, CURLOPT_URL, $request);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $out = curl_exec($curl);
    curl_close($curl);
}
$out = json_decode($out, true);*/
$out = json_decode(file_get_contents($request), true);
//$out['status'] = 'error';
//$out['status'] = 'success';
//$out['id'] = 1;
if ($out['status'] == 'success' && $out['id'] != 0) {
    $str = 'Заявка от ' .  $_POST['name'] . ' прошла СМС-верификацию успешно';
    $f=fopen("sending.log","a+");
    fputs($f, $str.' - '.date("Y-m-d H:i:s")."\r\n");
    fclose($f);
}elseif ($out['status'] == 'error'){
    $str = 'Заявка от ' .  $_POST['name'] . '. Ошибка при отправке. Текст: ' . (is_array($out['error'])? urldecode(http_build_query($out['error'])) : $out['error']);
    $str .= ' sms_id=' . $_POST['sms_id'] . ' sms_code='. $_POST['code'] . ' id=' . $_POST['id'];
    $f=fopen("sending.log","a+");
    fputs($f, $str.' - '.date("Y-m-d H:i:s")."\r\n");
    fclose($f);
    die(json_encode( array('success' => false)));
}elseif ($out['status'] == 'success' && $out['id'] == 0){
    $str = 'Заявка от ' .  $_POST['name'] . '. Результат: Подтверждение смс - отказано' ;
    $f=fopen("sending.log","a+");
    fputs($f, $str.' - '.date("Y-m-d H:i:s")."\r\n");
    fclose($f);
    die(json_encode( array('success' => 'fail')));
}else{
    $str = 'Заявка от ' .  $_POST['name'] . ' не прошла СМС-верификацию. ';
    $f=fopen("sending.log","a+");
    fputs($f, $str.' - '.date("Y-m-d H:i:s")."\r\n");
    fclose($f);
}
// echo $out;

die(json_encode( array('success' => true)));