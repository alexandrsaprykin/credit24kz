<?php
/** @var $this SenderViewMarks */
defined('_JEXEC') or die;// No direct access
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');
$user = JFactory::getUser();
$userId = $user->get('id');

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$saveOrder = $listOrder == 'ordering';


?>
<script type="text/javascript">
    Joomla.orderTable = function () {
        var table = document.getElementById("sortTable");
        var direction = document.getElementById("directionTable");
        var order = table.options[table.selectedIndex].value;
        if (order != '<?php echo $listOrder; ?>') {
            dirn = 'asc';
        } else {
            dirn = direction.options[direction.selectedIndex].value;
        }
        Joomla.tableOrdering(order, dirn, '');
    }


</script>

<form action="<?php echo JRoute::_('index.php?option=com_sender&view=statistics'); ?>" method="post" name="adminForm"
      id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
		<?php else : ?>
        <div id="j-main-container">
			<?php endif; ?>

			<?php
			// Search tools bar
			echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
			?>

            <table class="table table-striped" id="articleList">
                <thead>
                <tr>
                    <th width="1%">
						<?php echo JHtml::_('searchtools.sort', 'ID', 'id', $listDirn, $listOrder); ?>
                    </th>
                    <th width="10%" class="hidden-phone">
                        Имя
                    </th>
                    <th width="10%" class="nowrap">
                        Email
                    </th>
                    <th width="10%" class="nowrap">
                        Отправлено заявок за день
                    </th>
                    <th width="10%" class="nowrap">
                        Отправлено заявок за месяц
                    </th>
                </tr>
                </thead>
                <tbody>
				<?php foreach ($this->items as $i => $item) :

					?>
                    <tr class="row<?php echo $i % 2; ?>">
                        <td class="center hidden-phone">
							<?php echo $this->escape($item->id); ?>
                        </td>
                        <td class="nowrap has-context">
                            <div class="pull-left">
								<?php echo $this->escape($item->name); ?>
                            </div>
                        </td>
                        <td class="nowrap has-context">
                            <div class="pull-left">
								<?php echo JStringPunycode::emailToUTF8($this->escape($item->email)); ?>
                            </div>
                        </td>
                        <td class="hidden-phone">
							<?php echo $this->escape($item->day); ?>
                        </td>
                        <td class="hidden-phone">
							<?php echo $this->escape($item->month); ?>
                        </td>
                    </tr>
				<?php endforeach; ?>
                </tbody>
            </table>
			<?php echo $this->pagination->getListFooter(); ?>

            <input type="hidden" name="task" value=""/>
            <input type="hidden" name="boxchecked" value="0"/>
			<?php echo JHtml::_('form.token'); ?>

        </div>
</form>