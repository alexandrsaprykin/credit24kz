<!-- Задать вопрос -->
<div class="modal" tabindex="-1" role="dialog" id="question">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Задать вопрос</h4>
            </div>
            <div class="modal-body">
                <form class="question_form">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" value="Вопрос" data-name = "type" class="for-ajax-question">
                            <label for="full_name">Ваше Имя и Фамилия</label><br/>
                            <input type="text" name="name" id="full_name" data-name ="Имя" placeholder="Ваше Имя и Фамилия" class="for-ajax-question" required><br/>
                        </div>
                        <div class="col-md-6">
                            <label for="phone1">Телефон</label><br/>
                            <div class="phone_field">
                                <div style="width:200px;display: inline;">+7</div>
                                <input style="width: 170px;" name="phone2" data-name ="Телефон" type="text" value="" id="phone2" class="for-ajax-question" onkeydown="setMask(this,'(99) 999-99-99');">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="mail">Ваш e-mail</label>
                            <input type="email" name="mail" data-name ="Емейл" id="mail" class="for-ajax-question" placeholder="petrov@site.kz">
                        </div>
                        <div class="col-md-12">
                            <label for="message">Текст вопроса</label>
                            <textarea name="message" data-name ="Вопрос" class="for-ajax-question" id="message"></textarea>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="submit">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->