<!-- Оставить Отзыв -->
<div class="modal" tabindex="-1" role="dialog" id="sms-confirm2" style="display: block; top: 30%; " >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title">Будь ласка, введіть код, отриманий в смс!</h4>
            </div>
            <div class="modal-body">
                <form class="sms_form2">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" value="<?php echo $out['id']?>" data-name="id" class="for-ajax-sms">
                            <input type="hidden" value="<?php echo $out['sms_id']?>" data-name="sms_id" class="for-ajax-sms">
                            <input type="hidden" value="<?php echo $_POST['contact_name']?>" data-name="name" class="for-ajax-sms">
                            <label for="code" class="error" style="color: red; display: none;  margin-left: 100px;">Введено невірний код!</label>
                            <input type="text" class="for-ajax-sms" id="code_sms" name="code" data-name="code" required placeholder="Код з СМС" style=" display: block; margin: 0 auto; width: 80%; text-align: center;  margin-bottom: 15px;">
                        </div>

                        <div class="col-md-12">
                            <button type="submit" class="submit">Відправити</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->