<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 19.12.2018
 * Time: 17:47
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
	require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
	define('JPATH_BASE', preg_replace('/(\\\|\/)scripts$/', '', dirname(__FILE__)));
	require_once JPATH_BASE . '/includes/defines.php';
}
define('JPATH_COMPONENT', JPATH_BASE . '/components/com_sender');

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

// Load the configuration
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_BASE . '/includes/framework.php';

require_once JPATH_COMPONENT . '/helpers/sender.php';

$mainframe = JFactory::getApplication('site');
$mainframe->initialise();

$db = JFactory::getDbo();

jimport('joomla.application.component.model');
jimport('joomla.application.component.helper');

JModelLegacy::addIncludePath(JPATH_COMPONENT . '/sending', 'SendingModel');

senderSiteHelper::initLogger();

$ad_model = JModelLegacy::getInstance('Admitad', 'SendingModel');
$r_model = JModelLegacy::getInstance('Request', 'SendingModel');

$com_sender = JComponentHelper::getComponent('com_sender');
$params = new JRegistry($com_sender->getParams());
$ad_id = $params->get('admitad_id');
$site_name = JFactory::getConfig()->get('sitename');
$developer_mode = $params->get('developer_mode');

$from = strtotime('01.05.2019');
$to = strtotime('02.05.2019');

//var_dump($from, $to);

//creditDnepr
$name = 'CreditDnepr';
$requests = $r_model->getNotSendedByTime($from, $to);


var_dump(count($requests));
foreach ($requests as $request_) {
	$ad_model->send($request_, $ad_id, $developer_mode, false, array($name));
	var_dump($request_['id']);
}


