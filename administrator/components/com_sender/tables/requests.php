<?php

// No direct access
defined( '_JEXEC' ) or die;

/**
 * Object Class Table
 * @author Sapaleks
 */
class TableRequests extends JTable
{

	/**
	 * Class constructor
	 * @param Object $db (database link object)
	 */
	function __construct( &$db )
	{
		parent::__construct( '#__requests', 'id', $db );
	}

	/**
	 * Method for loading data into the object field
	 * @param Array $array (Featured in the field of data)
	 * @param String $ignore
	 * @return Boolean result
	 */
	public function bind( $array, $ignore = '' )
	{

		if ( empty( $array['date'] ) ) {
			$array['date'] = date( 'Y-m-d H:i:s' );
		}
		if ( isset( $array['rules'] ) && is_array( $array['rules'] ) ) {
			$rules = new JAccessRules( $array['rules'] );
			$this->setRules( $rules );
		}

		if ( isset( $array['params'] ) && is_array( $array['params'] ) )
		{
			$registry = new JRegistry;
			$registry->loadArray( $array['params'] );
			$array['params'] = (string) $registry;
		}

		return parent::bind( $array, $ignore );
	}

}