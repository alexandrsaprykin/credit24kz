<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="footer_logo"><img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/footer_logo.png" alt="logo"></div>
            </div>
            <div class="col-md-9 col-xs-12">
                <div class="footer_info">
                    <ul>
                            <li>+77273122833</li>
                            <li>+7715372658</li>
                            <li>+7078372325</li>
                        <li>Алматы, улица Толе Би, 83. Оф. 340</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-xs-4"><span>&copy;2018 Все права защищены</span></div>
                <div class="col-md-offset-3 col-md-3 col-xs-4">
                <div class="footer_map"><a href="#map" data-toggle="modal">Смотреть на карте</a></div>
            </div>
        </div>
    </div>
</footer>