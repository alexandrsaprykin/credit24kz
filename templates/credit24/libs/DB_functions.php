<?php
include 'DB.php';

class DBFunctions extends DB
{
    public function writeRequest($data = array())
    {
        $keys = array_keys($data);
        foreach ($keys as $key) {
            $pdo_keys[] = ':' . $key;
            $values[':' . $key] = $data[$key];
        }
        $id = $this->add("INSERT INTO `requests`(" . implode($keys, ', ') . ") VALUES(" . implode($pdo_keys, ', ') . ")", $values);
        return $id;
    }

    public function update($data = array(), $id)
    {
        $keys = array_keys($data);
        foreach ($keys as $key) {
            $values[':' . $key] = $data[$key];
            $parts[] = $key . '=' . ':' . $key;
        }
        $id = $this->set("UPDATE `requests` SET " . implode($parts, ', ') . " WHERE id = " . $id, $values);
    }

    public function getNotSended($delay = false)
    {
        if ($delay) {
            $query = 'SELECT * FROM `requests` WHERE date < ' . (time() - $delay) . ' AND sended = 0';
        } else {
            $query = 'SELECT * FROM `requests` WHERE sended = 0';
        }

        $data = $this->getAll($query);
        return $data;
    }

    public function getNotSendedByTime($from, $to)
    {

		$query = 'SELECT * FROM `requests` WHERE date BETWEEN ' . $from . ' AND ' . $to;
        $data = $this->getAll($query);
        return $data;
    }

    public function getNotLoged()
    {
        $query = 'SELECT confirm_result, id FROM `requests` WHERE confirm_result != "" AND confirm_result_log = 0 AND date < ' . (time() - 120);
        $data = $this->getAll($query);
        return $data;
    }

    public function getAdmitadCampaigns()
    {
        $query = 'SELECT * FROM `campaigns`';
        $data = $this->getAll($query);
        return $data;
    }

    public function updateCampaignsInfo()
    {
        $this->set("UPDATE `campaigns` SET curl_count = 0, ad_count = 0, day_limit = 0");
    }
}

