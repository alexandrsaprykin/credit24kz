<?php

// No direct access
defined('_JEXEC') or die;

/**
 * Model for edit/create current element
 * @author Sapaleks
 */
class SendingModelRequest extends JModelList
{

	public function writeRequest($data = array())
	{
		$keys = array_keys($data);
		foreach ($keys as $key) {
			$values[$key] = "'" . addslashes($data[$key]) . "'";
		}
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("INSERT INTO `#__requests`(`" . implode($keys, '`, `') . "`) VALUES(" . implode($values, ', ') . ")");
		//var_dump("INSERT INTO `#__requests`(`" . implode($keys, '`, `') . "`) VALUES(" . implode($values, ', ') . ")");
		$db->setQuery($query);
		$id = $db->execute();
		return $id;

	}

	public function writeScriptRequest($data)
	{
		$keys = array_keys($data);
		foreach ($keys as $key) {
			$values[$key] = '"' . $data[$key] . '"';
		}
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("INSERT INTO `#__script_requests`(" . implode($keys, ', ') . ") VALUES(" . implode($values, ', ') . ")");
		$db->setQuery($query);
		$id = $db->execute();
		return $id;
	}

	public function update($data = array(), $id)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$keys = array_keys($data);
		foreach ($keys as $key) {
			$parts[] = $key . '=' . $data[$key];
		}
		$query->setQuery("UPDATE `#__requests` SET " . implode($parts, ', ') . " WHERE id = " . $id);
		$db->setQuery($query);
		$db->execute();
	}

	public function updateNotSendedByCampaigns($ids = array(), $name)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$arr = implode($ids, ',');
		$query->setQuery('UPDATE `#__admitad_campaigns` SET requests_list = "' . $arr . '" WHERE method_name = "' . $name . '"');
		$db->setQuery($query);
		$db->execute();
	}

	public function getNotSendedByCampaigns($name)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		$r = 'SELECT requests_list FROM `#__admitad_campaigns` WHERE method_name = "' . $name . '"';

		$query->setQuery($r);
		$db->setQuery($query);
		$data = $db->loadAssoc();

		if ($data['requests_list']) {
			$data = explode(',', $data['requests_list']);
			return $data;
		} else return false;

	}

	public function scriptUpdate($data = array(), $id)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$keys = array_keys($data);
		foreach ($keys as $key) {
			$parts[] = $key . '=' . $data[$key];
		}
		$query->setQuery("UPDATE `#__script_requests` SET " . implode($parts, ', ') . " WHERE id = " . $id);
		$db->setQuery($query);
		$db->execute();
	}

	public function getNotSended($delay = false)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		if ($delay) {
			$r = 'SELECT * FROM `#__requests` WHERE date < ' . (time() - $delay) . ' AND sended = 0 OR delay = 0 AND sended = 0';
		} else {
			$r = 'SELECT * FROM `#__requests` WHERE sended = 0 LIMIT 50';
		}

		$query->setQuery($r);
		$db->setQuery($query);
		$data = $db->loadAssocList();
		return $data;
	}

	public function getRequests($from = 0, $to = 0)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		$r = 'SELECT * FROM `#__requests` WHERE date BETWEEN ' . $from . ' AND ' . $to;
		$query->setQuery($r);
		$db->setQuery($query);
		$data = $db->loadAssocList();

		return $data;
	}

	public function getRequests2()
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		$r = 'SELECT * FROM `#__requests2` WHERE id > 216257 AND id < 224244';
		$query->setQuery($r);
		$db->setQuery($query);
		$data = $db->loadAssocList();

		return $data;
	}

	public function getRequestsByIds($ids, $limit = 0)
	{
		if ($ids) {
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			$ids = implode($ids, ',');
			$r = 'SELECT * FROM `#__requests` WHERE id IN ( ' . $ids . ')';
			if ($limit) {
				$r .= ' LIMIT ' . $limit;
			}
			$query->setQuery($r);
			$db->setQuery($query);
			$data = $db->loadAssocList();
			return $data;
		} else return false;

	}

	public function getNotSendedByTime($from, $to)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$r = 'SELECT * FROM `#__requests` WHERE date BETWEEN ' . $from . ' AND ' . $to;
		$query->setQuery($r);
		$db->setQuery($query);
		$data = $db->loadAssocList();

		return $data;
	}

	public function getNotSendedScriptData()
	{

		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$r = 'SELECT * FROM `#__script_requests` WHERE sended = 0';
		$query->setQuery($r);
		$db->setQuery($query);
		$data = $db->loadAssocList();
		return $data;
	}

	public function getNotLoged()
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$r = 'SELECT confirm_result, id FROM `#__requests` WHERE confirm_result != "" AND confirm_result_log = 0 AND sended = 1';
		$query->setQuery($r);
		$db->setQuery($query);
		$data = $db->loadAssocList();

		return $data;
	}

	public function bankSendMark($bank_id, $request_id)
	{
		$data = [$bank_id, $request_id, 0, 0, 0];
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("INSERT INTO `#__bank_requests` VALUES(" . implode($data, ', ') . ")");
		$db->setQuery($query);
		$id = $db->execute();
		return $id;
	}

}