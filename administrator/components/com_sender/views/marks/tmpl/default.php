<?php
/** @var $this SenderViewMarks */
defined('_JEXEC') or die;// No direct access
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');
$user = JFactory::getUser();
$userId = $user->get('id');

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$saveOrder = $listOrder == 'ordering';

if ($saveOrder) {
	$saveOrderingUrl = 'index.php?option=com_sender&task=marks.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'articleList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
    Joomla.orderTable = function () {
        var table = document.getElementById("sortTable");
        var direction = document.getElementById("directionTable");
        var order = table.options[table.selectedIndex].value;
        if (order != '<?php echo $listOrder; ?>') {
            dirn = 'asc';
        } else {
            dirn = direction.options[direction.selectedIndex].value;
        }
        Joomla.tableOrdering(order, dirn, '');
    }


</script>

<form action="<?php echo JRoute::_('index.php?option=com_sender&view=marks'); ?>" method="post" name="adminForm"
      id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
		<?php else : ?>
        <div id="j-main-container">
			<?php endif; ?>

			<?php
			// Search tools bar
			echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
			?>

            <table class="table table-striped" id="articleList">
                <thead>
                <tr>
                    <th width="1%" class="center hidden-phone" nowrap="nowrap">
						<?php echo JHtml::_('searchtools.sort', '', 'ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING', 'icon-menu-2'); ?>
                    </th>
                    <th width="1%" class="hidden-phone">
                        <input type="checkbox" name="checkall-toggle" value=""
                               title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)"/>
                    </th>
                    <th >
						<?php echo JHtml::_('searchtools.sort', 'Название метки', 'title', $listDirn, $listOrder); ?>
                    </th>
                    <th width="10%" class="hidden-phone">
						<?php echo JHtml::_('searchtools.sort', 'Цвет', 'created_by', $listDirn, $listOrder); ?>
                    </th>
                </tr>
                </thead>
                <tbody>
				<?php foreach ($this->items as $i => $item) :
					$item->max_ordering = 0;
					$ordering = ($listOrder == 'a.ordering');
					$canEdit = $user->authorise('core.edit', '#__marks.' . $item->id);
					$canCheckin = $user->authorise('core.manage', 'com_sender') || $item->checked_out == $userId || $item->checked_out == 0;
					$canEditOwn = $user->authorise('core.edit.own', '#__marks.' . $item->id);
					$canChange = $user->authorise('core.edit.state', '#__marks.' . $item->id) && $canCheckin;
					?>
                    <tr class="row<?php echo $i % 2; ?>">
                        <td class="order nowrap center hidden-phone">
							<?php if ($canChange) :
								$disableClassName = '';
								$disabledLabel = '';
								if (!$saveOrder) :
									$disabledLabel = JText::_('JORDERINGDISABLED');
									$disableClassName = 'inactive tip-top';
								endif; ?>
                                <span class="sortable-handler <?php echo $disableClassName ?>"
                                      title="<?php echo $disabledLabel ?>" rel="tooltip"><i
                                            class="icon-menu"></i></span>
                                <input type="text" style="display:none" name="order[]" size="5"
                                       value="<?php echo $item->ordering; ?>" class="width-20 text-area-order "/>
							<?php else : ?>
                                <span class="sortable-handler inactive"><i class="icon-menu"></i></span>
							<?php endif; ?>
                        </td>
                        <td class="center hidden-phone">
							<?php echo JHtml::_('grid.id', $i, $item->id); ?>
                        </td>
                        <td class="nowrap has-context">
                            <div class="pull-left">
								<?php if ($canEdit || $canEditOwn) : ?>
                                    <a href="<?php echo JRoute::_('index.php?option=com_sender&task=mark.edit&id=' . $item->id); ?>"
                                       title="<?php echo JText::_('JACTION_EDIT'); ?>">
										<?php echo $this->escape($item->name); ?></a>
								<?php else : ?>
                                    <span title="<?php echo JText::sprintf('JFIELD_ALIAS_LABEL', $this->escape($item->name)); ?>"><?php echo $this->escape($item->name); ?></span>
								<?php endif; ?>

                            </div>

                        </td>
                        <td class="small hidden-phone center" style="background-color: <?php echo $this->escape($item->color); ?>">
							<?php echo $this->escape($item->color); ?>
                        </td>
                    </tr>
				<?php endforeach; ?>
                </tbody>
            </table>
			<?php echo $this->pagination->getListFooter(); ?>

            <input type="hidden" name="task" value=""/>
            <input type="hidden" name="boxchecked" value="0"/>
			<?php echo JHtml::_('form.token'); ?>

        </div>
</form>