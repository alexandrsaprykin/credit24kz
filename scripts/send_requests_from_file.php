<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 21.11.2018
 * Time: 20:45
 */
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//define("path", "/home/credit24org/public_html/");
define("path", "../separate_form_script_fl_idea/");
define("sending_debug", true);
define("sending_log_file", path . "sending.log");

include  '../config.php';
include_once  '../libs/DB_functions.php';

if (!function_exists('to_log')) {
	function to_log($str)
	{

		if (sending_debug) {
			$f = fopen(sending_log_file, "a+");
			fputs($f, $str . ' - ' . date("Y-m-d H:i:s") . "\r\n");
			fclose($f);
		}
		return false;
	}
}

$db = new DBFunctions(DB_NAME, DB_HOST, DB_USER, DB_PASS);

$from = strtotime("14.11.2018 11:00"); //дата и время с которого начать отправлять заявки
$to = strtotime("14.11.2018 16:20");  //дата и время до которого  отправлять заявки
//$to = time();
$file_str = file( '../templates/orders.txt');
//var_dump($file_str);
$all_data = array();
$num = 0;
foreach ($file_str as $str) {
	$pos = strpos($str, ":");
	$type = substr($str, 0, $pos);
	$value = substr($str, $pos + 1);
	//echo $type . ' - ' . $value . '<br>';

	switch (trim($type)) {
		case 'ФИО':
			$all_data[$num]['name'] = trim($value);
			break;
		case 'Телефон':
			$all_data[$num]['phone'] = substr(preg_replace("/[\(\)]/ui", "", trim($value)), -10);
			break;
		case 'Идентификационный код':
			$all_data[$num]['inn'] = trim($value);
			break;
		case 'Возраст':
			$all_data[$num]['age'] = trim($value);
			break;
		case 'Область фактического проживания':
			$all_data[$num]['obl'] = trim($value);
			break;
		case 'Тип дохода':
			$all_data[$num]['tip'] = trim($value);
			break;
		case 'Сумма кредита':
			$all_data[$num]['sum'] = (int)trim(preg_replace("/(,)|(грн.)/ui", "", $value));
			break;
	}

	if ($all_data[$num]['name'] && $all_data[$num]['phone'] && $all_data[$num]['phone'] &&
		$all_data[$num]['inn'] && $all_data[$num]['age'] && $all_data[$num]['obl']
		&& $all_data[$num]['tip'] && $all_data[$num]['sum']
	) {
		$all_data[$num]['referer'] = '';
		$all_data[$num]['istok'] = 'scriptpovtor';
		$num++;
	}

}


//отправка заявок


foreach ($all_data as $key => $data) {


	to_log('----------------------------');
	to_log('Скрипт - повтор. Обработка заявки от ' . $data['name'] . ' (' . $data['phone'] . ')  ИНН: ' . $data['inn']);

	var_dump($data);
	include '../admitad_sending.php';
	echo $data['name'] . ' - отправлено<br>';
	to_log('конец обработки');

}
