<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 19.12.2018
 * Time: 17:47
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
	require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
	define('JPATH_BASE', preg_replace('/(\\\|\/)scripts$/', '', dirname(__FILE__)));
	require_once JPATH_BASE . '/includes/defines.php';
}
define('JPATH_COMPONENT', JPATH_BASE . '/components/com_sender');

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

// Load the configuration
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_BASE . '/includes/framework.php';

require_once JPATH_COMPONENT . '/helpers/sender.php';

$mainframe = JFactory::getApplication('site');
$mainframe->initialise();

$db = JFactory::getDbo();

jimport('joomla.application.component.model');
jimport('joomla.application.component.helper');

JModelLegacy::addIncludePath(JPATH_COMPONENT . '/sending', 'SendingModel');

senderSiteHelper::initLoggerSms();

$ad_model = JModelLegacy::getInstance('Admitad', 'SendingModel');
$com_sender = JComponentHelper::getComponent('com_sender');
$params = new JRegistry($com_sender->getParams());
$ad_id = $params->get('admitad_id');
$ad_id = 796696;
$developer_mode = $params->get('developer_mode');

$delay = 15 * 60; // 15 минут в секундах
$time = time() - $delay;

$query = "SELECT info FROM #__logger_info WHERE logger = 'sms'";
$db->setQuery($query);
$result = $db->loadAssoc();

$query = "SELECT * FROM #__requests WHERE id > " . (int)$result["info"] . " AND date < " . $time . " AND alfa_result LIKE '%Успешно%'  AND confirm_result = '' ORDER BY id";
$db->setQuery($query);
$result = $db->loadAssocList();

$last_id = 0;

foreach ($result as $key => $order) {
	$last_id = $order['id'];
	$str = date('Y-m-d H:i:s', $order['date']) . ' ' . $order['name'] . ' (' . $order['phone'] . ') ИНН: ' . $order['inn'];
	JLog::add($str, \JLog::DEBUG, 'com_sender_sms');
}

if ($last_id) {
	$query = "UPDATE `#__logger_info` SET info = " . $last_id . " WHERE logger = 'sms'";
}

$db->setQuery($query);
$result = $db->execute();

/*foreach ($list['results'] as $key => $order){
	var_dump($order);
	echo '<br>';
	echo '---------------------------';
	echo '<br>';
}
*/