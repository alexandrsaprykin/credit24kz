<?php
// No direct access
defined('_JEXEC') or die;

/**
 * Controller
 * @author Sapaleks
 */
class SenderControllerForm extends JControllerForm
{

	/**
	 * Class constructor
	 * @param array $config
	 */
	function __construct($config = array())
	{
		$this->view_list = 'Form';
		parent::__construct($config);
	}

	/**
	 * @return bool
	 */

	public function smsConfirm()
	{
		$data = $this->input->post->getArray();
		//die(json_encode($data));
		$model = $this->getModel();
		//получаем нашу форму
		$result = $model->checkSMSCode($data);

		die($result);

	}

	public function mail()
	{
		$mailer = JFactory::getMailer();
		$mailer->isHTML(true);
		$site_name = JFactory::getConfig()->get('sitename');
		$data = $this->input->post->getArray();
		$msg = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>";

		foreach ($data as $key => $value) {
			$msg = $msg . '<b>' . $key . ': </b>' . $value . '<br>';
		}
		$msg = $msg . '</body></html>';

		$mailer->setBody($msg);
		$mailer->setSubject($data['type'] . " " . $site_name);
		$mailer->addRecipient(array('vsi.ok.zayavka@yandex.ua', 'vsi.ok.zayavka@gmail.com'));

		$mailer->Send();

	}

	public function sendRequestsToScript()
	{
		$data = $this->input->post->getArray();
		$model = $this->getModel();
		$model->sendRequestsToScript($data['ids']);

		echo '124';

		// Close the application
		JFactory::getApplication()->close();
	}

	public function newRequest()
	{
		//Получаем наше приложение
		$app = JFactory::getApplication();
		//Данные коотрые пришли из формы
		$data = $this->input->post->getArray();
		/*$data = array(
			'date' => '1537779399',
			'contact_name' => 'Джумла Тест',
			'phone' => '0990000000',
			'phone2' => '',
			'idencod' => '1111111111',
			'age' => '26',
			'obl' => 'Житомирская область',
			'tip' => 'Я получаю пенсию',
			'duration' => '0',
			'tip2' => '',
			'summa' => '20000',
			'referer' => 'другой источник\r\n',
			'istok' => 'Sourcenotdefined',
			'device' => 'Компьютер',
			'alfa_result' => 'Успешно',
			'confirm_result' => 'Заявка от Тест Тест ПРОШЛА СМС-ВЕРИФИКАЦИЮ УСПЕШНО!!!-----------@@@@@@@@@@@@@@@@@@@@@@@@@@',
			'sended' => 1,
			'log' => '',
			'credit_type' => '',
			'confirm_result_log' => 1
		);*/
		//Получаем нашу модель

		if ($data['contact_name']) {
			$model = $this->getModel();

			$url = $model->saveRequest($data);

			if ($url) {
				$this->setRedirect($url);
			}
		}

		return 0;
	}

}