<!-- Оставить Отзыв -->
<div class="modal" tabindex="-1" role="dialog" id="sms-confirm" style="display: block; top: 27%;" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title">Пожалуйста, введите код, полученный в смс!</h4>
            </div>
            <div class="modal-body">
                <form class="sms_form">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" value="<?php echo $_GET['id']?>" data-name="id" class="for-ajax-sms">
                            <input type="hidden" value="<?php echo $_GET['sms_id']?>" data-name="sms_id" class="for-ajax-sms">
                            <input type="hidden" value="<?php echo $_GET['db_id']?>" data-name="db_id" class="for-ajax-sms">
                            <input type="hidden" value="<?php echo $_GET['contact_name']?>" data-name="name" class="for-ajax-sms">
                            <input type="hidden" value="<?php echo $_GET['cash']?>" data-name="cash" class="for-ajax-sms">
                            <input type="hidden" value="<?php echo $_GET['inn']?>" data-name="inn" class="for-ajax-sms">
                            <input type="hidden" value="<?php echo $_GET['phone']?>" data-name="phone" class="for-ajax-sms">
                            <label for="code_sms" class="error" style="color: red; display: none;  margin-left: 100px;">Введен неверный код!</label>
                            <input type="text" class="for-ajax-sms" id="code_sms" name="code" data-name="code" required placeholder="Код из СМС" style=" display: block; margin: 0 auto; width: 80%; text-align: center;  margin-bottom: 15px;font-size: 25px;letter-spacing: 10px;">
                        </div>

                        <div class="col-md-12">
                            <button type="submit" class="submit">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    jQuery(function($){
        $("#code_sms").mask("9999");
    });
</script>