<?php
/** @var $this SenderViewRequests */
defined('_JEXEC') or die;// No direct access
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');
$user = JFactory::getUser();
$userId = $user->get('id');

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$saveOrder = $listOrder == 'ordering';

if ($saveOrder) {
	$saveOrderingUrl = 'index.php?option=com_sender&task=requests.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'articleList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
    Joomla.orderTable = function () {
        var table = document.getElementById("sortTable");
        var direction = document.getElementById("directionTable");
        var order = table.options[table.selectedIndex].value;
        if (order != '<?php echo $listOrder; ?>') {
            dirn = 'asc';
        } else {
            dirn = direction.options[direction.selectedIndex].value;
        }
        Joomla.tableOrdering(order, dirn, '');
    }

    jQuery(function ($) {
        //$( document ).ready(function() {console.log( $(document.adminForm).find('#bank_chzn'));
            //$(document.adminForm).find('#bank_chzn li[data-option-array-index="' + document.adminForm.filter_bank.value + '"]').click();
           // $(document.adminForm.bank).val(document.adminForm.filter_bank.value);
           // $(document.adminForm.bank).find('option[value="' + document.adminForm.filter_bank.value + '"]').attr('selected', 'selected');
           // console.log($(document.adminForm.bank).find('option[value="' + document.adminForm.filter_bank.value + '"]'));
           // $(document.adminForm.bank_status).val(document.adminForm.filter_bank_status.value);
        //});
        $(document.adminForm.bank).change(function () {
            $(document.adminForm.filter_bank).val($(document.adminForm.bank).find(":selected").val());
            document.adminForm.submit();
        });
        $(document.adminForm.bank_status).change(function () {
            $(document.adminForm.filter_bank_status).val($(document.adminForm.bank_status).find(":selected").val());
            document.adminForm.submit();
        });

        $('.search-choice-close').click(function () {

            $.post('index.php?option=com_sender&task=mark.deleteMark&tmpl=component',
                {
                    'request_id': $(this).attr('data-r-id'),
                    'mark_id': $(this).attr('data-mark-id')
                },
                function (response, status) {
                    //if (status = 'success') {
                    location.reload();
                    //}
                }
            );
        });

        $('button.button-redo').click(function () {
            var checked = $('#adminForm td input[type=checkbox]:checked'),
                arrchecked = [];

            if (document.adminForm.boxchecked.value == 0) {
                alert(Joomla.JText._('JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST'));
                return false;
            }

            checked.each(function (index) {
                arrchecked[index] = $(this).val();
            });

            Joomla.submitbutton('newsfeed.apply');
            $.post('/index.php?option=com_sender&view=form&task=form.sendRequestsToScript',
                {
                    'ids': arrchecked
                },
                function (response, status) {
                    console.log(response);
                    console.log(status);
                    //if (status = 'success') {
                    location.reload();
                    //}
                }
            );
        });
    });

    Joomla.addMarks = function () {

        jQuery(function ($) {
                var checked = $('#adminForm td input[type=checkbox]:checked'),
                    arrchecked = [],
                    mark_id = $('#mark_action').val();
                checked.each(function (index) {
                    arrchecked[index] = $(this).val();
                });

                if (!mark_id) {
                    alert('Выберите, пожалуйста тип метки!');
                    return false;
                }

                $.post('index.php?option=com_sender&task=mark.addMarks&tmpl=component',
                    {
                        'request_ids': arrchecked,
                        'mark_id': mark_id
                    },
                    function (response, status) {
                        //if (status = 'success') {
                        location.reload();
                        //}
                    }
                );

            }
        )
        return false;
    };

    Joomla.deleteMarks = function () {

        jQuery(function ($) {
                var checked = $('#adminForm td input[type=checkbox]:checked'),
                    arrchecked = [],
                    mark_id = $('#mark_action').val();
                checked.each(function (index) {
                    arrchecked[index] = $(this).val();
                });

                if (!mark_id) {
                    alert('Выберите, пожалуйста тип метки!');
                    return false;
                }

                $.post('index.php?option=com_sender&task=mark.deleteMarks&tmpl=component',
                    {
                        'request_ids': arrchecked,
                        'mark_id': mark_id
                    },
                    function (response, status) {
                        //if (status = 'success') {
                        location.reload();
                        //}
                    }
                );

            }
        )
        return false;
    };
</script>

<form action="<?php echo JRoute::_('index.php?option=com_sender&view=requests'); ?>" method="post" name="adminForm"
      id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
		<?php else : ?>
        <div id="j-main-container">
			<?php endif; ?>

			<?php
			// Search tools bar
			echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
			?>

            <div class="" style="margin-bottom: 15px; margin-top: 5px; min-height: 45px">
                <div class="container-fluid" style="padding-left: 0;">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="btn-toolbar" role="toolbar" id="toolbar">

                                <select name="bank" id="bank">
                                    <option value="" selected>- Выбрать банк -</option>
                                    <?
                                    foreach ($this->banks as $bank) {
                                        echo '<option value="' . $bank->id . '" ' . (($this->filterForm->getValue('bank', 'filter') == $bank->id)? 'selected' : '') . '>' . $bank->alias;
                                    }
                                    ?>
                                </select>


                                <div class="btn-wrapper" id="toolbar-add" <? if (!$this->filterForm->getValue('bank', 'filter')) {?>style="display: none;"<?}?>>
                                    <select name="bank_status" id="bank_status">
                                        <option value="" <?php if (!$this->filterForm->getValue('bank_status', 'filter')) {echo 'selected';} ?>>Все заявки</option>
                                        <option value="sended" <?php if ($this->filterForm->getValue('bank_status', 'filter') == 'sended') {echo 'selected';} ?>>Обработанные</option>
                                        <option value="not_sended" <?php if ($this->filterForm->getValue('bank_status', 'filter') == 'not_sended') {echo 'selected';} ?>>Необработанные</option>
                                    </select>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="btn-toolbar" role="toolbar" id="toolbar">
                                <select name="mark_action" id="mark_action">
                                    <option value="" selected>Выбрать метку для действия</option>
									<?
									foreach ($this->marks as $mark) {
										echo '<option value="' . $mark->id . '">' . $mark->name;
									}
									?></select>
                                <div class="btn-wrapper" id="toolbar-add">
                                    <button onclick="if (document.adminForm.boxchecked.value == 0) { alert(Joomla.JText._('JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST')); } else {Joomla.addMarks(); }"
                                            class="btn btn-small">
                                        <span class="icon-new" aria-hidden="true"></span>
                                        Добавить метку
                                    </button>
                                </div>
                                <div class="btn-wrapper" id="toolbar-delete">
                                    <button onclick="if (document.adminForm.boxchecked.value == 0) { alert(Joomla.JText._('JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST')); } else {Joomla.deleteMarks(); }"
                                            class="btn btn-small">
                                        <span class="icon-delete" aria-hidden="true"></span>
                                        Удалить метку
                                    </button>
                                </div>
                                <div class="btn-wrapper" id="toolbar-submit" style="float: right;">
                                    <button onclick="document.adminForm.submit()"
                                            class="btn btn-small">
                                        <span class="icon-search" aria-hidden="true"></span>
                                        Поиск
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <span style="background-color: #F0F0F0; padding: 4px">Найдено <?php echo $this->total;?> результатов</span>
            </div>
            <table class="table table-striped" id="articleList">
                <thead>
                <tr>
                    <th width="1%" class="hidden-phone">
                        <input type="checkbox" name="checkall-toggle" value=""
                               title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)"/>
                    </th>
                    <th width="1%" class="nowrap hidden-phone">
						<?php echo JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'id', $listDirn, $listOrder); ?>
                    </th>
                    <th width="5%" style="min-width:55px" class="hidden-phone">
						<?php echo JHtml::_('searchtools.sort', 'Дата', 'date', $listDirn, $listOrder); ?>
                    </th>
                    <th width="15%">
						<?php echo JHtml::_('searchtools.sort', 'ФИО', 'name', $listDirn, $listOrder); ?>
                    </th>
                    <th width="10%" class="hidden-phone">
						<?php echo JHtml::_('searchtools.sort', 'Телефон', 'phone', $listDirn, $listOrder); ?>
                    </th>
                    <th width="10%" class="hidden-phone">
						<?php echo JHtml::_('searchtools.sort', 'ИНН', 'inn', $listDirn, $listOrder); ?>
                    </th>
                    <th width="5%" class="hidden-phone">
						<?php echo JText::_('Альфа-адмитад'); ?>
                    </th>
                    <th width="5%" class="hidden-phone">
						<?php echo JText::_('СМС'); ?>
                    </th>
                    <!--<th width="1%" class="nowrap hidden-phone">
						<?php echo JHtml::_('searchtools.sort', 'Адмитад', 'admitad', $listDirn, $listOrder); ?>
                    </th>-->
                    <th width="1%" class="nowrap hidden-phone">
						<?php echo JText::_('Метки'); ?>
                    </th>
                </tr>
                </thead>
                <tbody>
				<?php foreach ($this->items as $i => $item) :
					$item->max_ordering = 0;
					$ordering = ($listOrder == 'a.ordering');
					?>
                    <tr class="row<?php echo $i % 2; ?>">
                        <td class="center hidden-phone">
							<?php echo JHtml::_('grid.id', $i, $item->id); ?>
                        </td>
                        <td class="center hidden-phone">
                            <a href="<?php echo JRoute::_('index.php?option=com_sender&task=request.edit&id=' . $item->id); ?>"
                               title="<?php echo JText::_('JACTION_EDIT'); ?>">
								<?php echo $this->escape($item->id); ?></a>
                        </td>
                        <td class="nowrap small hidden-phone">
							<?php echo JHtml::_('date', $item->date, JText::_('DATE_FORMAT_LC6')); ?>
                        </td>
                        <td class="small hidden-phone">
							<?php echo $this->escape($item->name); ?>
                        </td>
                        <td class="small hidden-phone">
							<?php echo $this->escape($item->phone); ?>
                        </td>
                        <td class="small hidden-phone">
							<?php echo $this->escape($item->inn); ?>
                        </td>
                        <td class="small hidden-phone">
							<?php if (stristr($item->alfa_result, "Успешно")) {
								$class = 'icon-publish';
								$text = 'Принят';
							} elseif (stristr($item->alfa_result, "Отказано") || stristr($item->alfa_result, "Дубль")) {
								$class = 'icon-smiley-sad';
								$text = 'Отклонен';
							} elseif (stristr($item->alfa_result, "Ошибка")) {
								$class = 'icon-unpublish';
								$text = 'Ошибка';
							} else {
								$class = 'icon-clock';
								$text = 'Данных нет';
							}
							?>
                            <span class="<?php echo $class; ?>" title="<?php echo $text; ?>"></span>

                        </td>
                        <td class="hidden-phone">

							<?php
							if (stristr($item->alfa_result, "Успешно")) {
								if (!$item->confirm_result) {
									$class = 'icon-ban-circle';
									$text = 'Не ввел смс';
								} elseif (stristr($item->confirm_result, "Успешно") || stristr($item->confirm_result, "УСПЕШНО")) {
									$class = 'icon-publish';
									$text = 'Принят';
								} else {
									$class = 'icon-unpublish';
									$text = 'Ошибка';
								}
								?>
                                <span class="<?php echo $class; ?>" title="<?php echo $text; ?>"></span>
							<?php } ?>
                        </td>
                        <!--<td class="center">
							<?php if (isset($item->admitad) && $item->admitad) {
							foreach ($item->admitad as $key => $results) :
								switch ($results->result) {
									case 1:
										$class = 'icon-publish';
										$text = 'Принят';
										break;
									case 2:
										$class = 'icon-smiley-sad';
										$text = 'Отклонен';
										break;
									case 3:
										$class = 'icon-unpublish';
										$text = 'Ошибка';
										break;
									default:
										$class = 'icon-clock';
										$text = 'Данных нет';
										break;
								}
								?>
                                    <span class="<?php echo $class; ?>" title="<?php echo $results->name; ?>"></span>
								<?php endforeach;
						} ?>
                        </td>-->
                        <td class="center">
							<?php if (isset($item->marks) && $item->marks) { ?>
                                <div class="chzn-container chzn-container-multi">
                                    <ul class="chzn-choices">
										<? foreach ($item->marks as $key => $mark) : ?>
                                            <li class="search-choice"><span><?php echo $mark->name; ?></span><a
                                                        class="search-choice-close"
                                                        data-mark-id="<?php echo $mark->id; ?>"
                                                        data-r-id="<?php echo $item->id; ?>"></a></li>

										<?php endforeach; ?>
                                    </ul>
                                </div>
							<? } ?>
                        </td>

                    </tr>
				<?php endforeach; ?>
                </tbody>
            </table>
			<?php echo $this->pagination->getListFooter(); ?>

            <input type="hidden" name="task" value=""/>
            <input type="hidden" name="boxchecked" value="0"/>
			<?php echo JHtml::_('form.token'); ?>

        </div>
</form>