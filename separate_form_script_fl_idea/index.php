<?php if (isset($_POST['contact_name'])) require_once('init_joomla.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Separate form script</title>
    <link rel="stylesheet" media="screen" href="styles.css">
    <script>
        function parseform(f) {

            var linesArr = document.getElementById('message').value.split(/\x0D\x0A|\x0A/);
            linesArr.forEach(function (item, i, arr) {

                if (item.indexOf("ФИО:") > -1) {
                    curstr = item.substr(item.indexOf(":") + 1).trim();
                    document.getElementById('contact_name').value = curstr;
                }

                if (item.indexOf("Телефон:") > -1 || item.indexOf("Тел.:") > -1) {
                    curstr = item.substr(item.indexOf(":") + 1).trim();
                    document.getElementById('phone').value = curstr;
                }

                if (item.indexOf("E-mail:") > -1 || item.indexOf("mail.:") > -1) {
                    curstr = item.substr(item.indexOf(":") + 1).trim();
                    document.getElementById('email').value = curstr;
                }

                if (item.indexOf("Идентификационный код:") > -1 || item.indexOf("ИНН:") > -1) {
                    curstr = item.substr(item.indexOf(":") + 1).trim();
                    document.getElementById('idencod').value = curstr;
                }

                if (item.indexOf("Возраст:") > -1) {
                    curstr = item.substr(item.indexOf(":") + 1).trim();
                    document.getElementById('age').value = curstr;
                }

                if (item.indexOf("Область фактического проживания:") > -1 || item.indexOf("Область:") > -1) {
                    curstr = item.substr(item.indexOf(":") + 1).trim();
                    document.getElementById('obl').value = curstr;
                }

                if (item.indexOf("Тип дохода:") > -1 || item.indexOf("Трудоустройство:") > -1) {
                    curstr = item.substr(item.indexOf(":") + 1).trim();
                    document.getElementById('tip').value = curstr;
                }

                if (item.indexOf("Сумма кредита:") > -1) {
                    curstr = item.substr(item.indexOf(":") + 1).trim();
                    if (curstr.indexOf("грн.") > -1) {
                        curstr = curstr.substr(0, curstr.indexOf("грн.")).trim();
                    }
                    document.getElementById('summa').value = curstr;
                }

                if (item.indexOf("Источник заявки:") > -1) {
                    curstr = item.substr(item.indexOf(":") + 1).trim();
                    document.getElementById('source').value = curstr;
                }

            });

        }
    </script>
</head>
<body>
<div class="container">
    <h1 style="color: red; text-align: center; margin-bottom: 50px">І-Ф</h1>
    <div class="left-side">
        <form class="separate_form" action="#" method="post" name="pre_separate_form"
              onsubmit="parseform(this); return false;">
            <ul>
                <li>
                    <h2>Текст з листа</h2>
                </li>
                <li>
                    <label for="message">Введiть даннi з листа i натиснiть "Опрацювати"</label>
                    <textarea name="message" id="message"></textarea>
                </li>
                <li>
                    <button class="submit" type="submit">Опрацювати -></button>
                </li>
            </ul>
        </form>
		<?php if (isset($_POST['contact_name'])) require_once('_sending.php'); ?>
    </div>

    <div class="right-side">
        <form class="separate_form" action="#" method="post" name="separate_form">
            <ul>
                <li>
                    <h2>Форма заявки</h2>
                </li>
                <li>
                    <label for="contact_name">ПІБ:</label>
                    <input id="contact_name" name="contact_name" type="text"/>
                </li>
                <li>
                    <label for="phone">Номер телефону:</label>
                    <input id="phone" name="phone" type="text"/>
                </li>
                <li>
                    <label for="email">E-mail:</label>
                    <input id="email" name="email" type="text"/>
                </li>
                <li>
                    <label for="idencod">Ідентифікаційний код:</label>
                    <input id="idencod" name="idencod" type="text"/>
                </li>
                <li>
                    <label for="age">Вік:</label>
                    <input id="age" name="age" type="text"/>
                </li>
                <li>
                    <label for="obl">Область фактичного проживання:</label>
                    <input id="obl" name="obl" type="text"/>
                </li>
                <li>
                    <label for="tip">Тип доходу:</label>
                    <input id="tip" name="tip" type="text"/>
                </li>
                <li>
                    <label for="summa">Сума кредиту:</label>
                    <input id="summa" name="summa" type="text"/>
                </li>
                <li>
                    <label for="source">Рекламна компанія:</label>
                    <input id="source" name="source" type="text"/>
                </li>
                <li>
                    <label for="worker_id">Код працівника:</label>
                    <input id="worker_id" name="worker_id" type="text"/>
                </li>
                <li>
                    <button class="submit" type="submit">Вiдправити</button>
                </li>
            </ul>
        </form>
    </div>
</div>

</body>
</html>