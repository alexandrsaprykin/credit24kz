<!-- Перезвоните -->
<div class="modal" tabindex="-1" role="dialog" id="recall">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Перезвоните мне</h4>
            </div>
            <div class="modal-body">
                <form class="recall_form">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" value="Перезвоните мне" data-name = "type" class="for-ajax-recall">
                            <label for="full_name">Ваше Имя и Фамилия</label><br/>
                            <input type="text" name="name" id="full_name" placeholder="Ваше Имя и Фамилия" data-name = "Имя" class="for-ajax-recall" required>
                        </div>
                        <div class="col-md-6">
                            <label for="phone1">Телефон</label>
                            <div class="phone_field">
                                <div style="width:200px;display: inline;">+7</div>
                                <input style="width:170px;" name="phone1" type="text" value="" id="phone1" onkeydown="mask('(99) 999-99-99');" data-name = "Телефон" class="for-ajax-recall" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="submit">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
