<!-- Modal Заявка отправлена -->
<div class="modal" tabindex="-1" role="dialog" id="thank">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ваша заявка отправлена!</h4>
            </div>
            <div class="modal-body">
                <p class="service_info">В ближайшее время с Вами свяжется<br/> наш менеджер для уточнения деталей.</p>
                <p class="service_desk">Спасибо за использование нашего сервиса!</p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->