<!-- Заявка на кредит -->
<div class="modal" tabindex="-" role="dialog" id="credit">
    <div class="modal-dialog" role="document">
        <!-- <div class="modal-content">-->
        <div class="modal-header">
            <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						 aria-hidden="true">&times;</span></button>-->
            <h4 class="modal-title">Заявка на кредит</h4>
            <p class="modal-desk">успейте оставить заявку
                <span class="green_price">до <?php echo((date('j', time() + 86400)) . ' ' . $monthes[(date('n', time() + 86400))]); ?></span>
                2019 – получите дополнительную <span class="green_price">скидку 5%</span>
                по ставке</p>
        </div>
        <div class="modal-body">
            <form class="contact-form" method="post"
                  action="<?php echo JRoute::_('/index.php?option=com_sender&view=form&task=form.newRequest') ?>"
                  onSubmit="return checkedForm(this)">
                <div class="row">
                    <div class="col-md-12">
                        <label for="full_name">Фамилия Имя Отчество</label><br/>
                        <label for="full_name" id="name_error" class="error">Укажите полностью свою фамилию, имя и
                            отчество</label>
                        <input type="text" name="contact_name" id="full_name" class="for-ajax" required><br/>
                    </div>
                    <div class="col-md-6">
                        <label for="phone">Телефон</label><br/>
                        <div class="phone_field">
                            <div style="width:200px;display: inline;font-size: 18px;">+7</div>
                            <input style="" placeholder="(___) ___-__-__" name="phone" type="text" value=""
                                   id="phone" class="for-ajax" required>
                        </div>
                    </div>
                   <!-- <div class="col-md-6">
                        <label for="email">E-mail (не обязательно)</label><br/>-->
                        <input style="" placeholder="qwerty@gmail.com" name="email" type="hidden" value=""
                               id="email" class="for-ajax">
                    <!--</div>-->

                    <div class="col-md-12">
                        <label for="code">Индивидуальный идентификационный номер(ИИН)</label><br/>
                        <input type="text" name="idencod" id="code" class="for-ajax" required><br/>
                        <div id="inn_check_block">
                            <input type="checkbox" id="confirm_inn" class="checkbox" name="inn_dis">
                            <label class="label" for="confirm_inn" id="confirm_text">Я настаиваю на том что ИИН указан правильно</label>
                        </div>
                    </div>



                    <div class="col-md-12">
                        <label for="tip">Тип дохода</label><br/>
                        <select name="tip" id="tip" class="for-ajax" required>
                            <option value="">Выберите тип дохода</option>
                            <option value="Я работаю официально" id="official">Я работаю официально</option>
                            <option value="Я получаю пенсию">Я получаю пенсию</option>
                            <option value="Я частный предприниматель">Я индивидуальный предприниматель</option>
                            <option value="Я работаю неофициально">Я работаю неофициально</option>
                            <option value="Я не работаю">Я не работаю</option>
                        </select>
<!--                        <select name="work_duration" id="tip2" class="for-ajax" style="display: none;">-->
<!--                            <option value="">Как давно работаете на последнем месте работы?</option>-->
<!--                            <option value="официально больше 3">больше 3 месяцев на последнем месте работы</option>-->
<!--                            <option value="официально меньше 3">меньше 3 месяцев на последнем месте работы</option>-->
<!--                        </select>-->
<!--                        <select name="reg_duration" id="tip3" class="for-ajax" style="display: none;">-->
<!--                            <option value="">Как давно Вы зарегистрированы как ЧП?</option>-->
<!--                            <option value="больше 1 года">зарегистрирован как предприниматель больше 1 года</option>-->
<!--                            <option value="меньше 1 года">зарегистрирован как предприниматель меньше 1 года</option>-->
<!--                        </select>-->
                        <label for="sum2">Сумма кредита в тенге</label><br/>
                        <input type="text" name="summa" id="sum2" class="input_sum for-ajax" required>
                        <div id="fsogl">
                            <input type="checkbox" checked id="confirm" class="checkbox" name="sogl" required>
                            <label class="label" for="confirm" id="confirm_text">Я даю согласие на обработку
                                персональных данных и соглашаюсь с <a href="#agree" data-toggle="modal"
                                                                      target="_blank" class="agree">пользовательским
                                    соглашением</a></label>
                        </div>
                        <input type='hidden' name='device' id='device' class="for-ajax" value='Не определено'/>
                        <input type='hidden' name='referer' class="for-ajax" value='<? echo $_SESSION['ref']; ?>'/>
                        <input type='hidden' id="trigger" class="for-ajax" name='sended' value='false'/>
                        <input type='hidden' id="age" class="for-ajax" name='age' value='0'/>
                        <input type='hidden' class="for-ajax" name='ok' value='order'/>
                        <input type='hidden' name='istok' class="for-ajax" value='<?
						$ref = $_SERVER['REQUEST_URI'];
						$ref2 = $_SERVER['HTTP_REFERER'];
						if (strpos($ref, 'utm_campaign')) {
							$istok = substr($ref, strpos($ref, 'utm_campaign') + 13);
							if (strpos($istok, '&')) {
								$istok = substr($istok, 0, strpos($istok, '&'));
								setcookie("istok", $istok);
							}
						} elseif (strpos($ref2, 'utm_campaign')) {
							$istok = substr($ref2, strpos($ref2, 'utm_campaign') + 13);
							if (strpos($istok, '&')) {
								$istok = substr($istok, 0, strpos($istok, '&'));
								setcookie("istok", $istok);
							}
						} else {
							$istok = 'Sourcenotdefined';
							if ($_COOKIE['istok']) {
								$istok = $_COOKIE['istok'];
							}
						}
						echo $istok; ?>'/>
                    </div>
                    <div class="col-md-12">
                        <button name="form_button" type="submit" class="submit">Отправить</button>
                    </div>
                </div>
            </form>
        </div>
        <!--</div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script>

    // проверка формы -----------------------------------

    function checkedForm(form) {
        var RegEx = /\s/g;
        var RegEx2 = /\(|\)|\s|-/g;
        var RegEx3 = /^(\+380|380|038)/g;
        var d = 1;

        if (form.contact_name.value == "") {
            form.contact_name.style.border = "1px solid #FF0000";
            d = 0;
        } else {
            form.contact_name.style.border = "1px solid #CCCCCC";
        }

        var name_arr = form.contact_name.value.split(' ');
        if (name_arr.length > 1 && name_arr[0].replace(RegEx2, "") != '' && name_arr[1].replace(RegEx2, "") != '') {
            $('label#name_error').css('display', 'none');
        }
        else {
            form.contact_name.scrollIntoView(false);
            form.contact_name.focus();
            form.contact_name.style.border = "1px solid #FF0000";
            d = 0;
            $('label#name_error').css('display', 'block');
        }

        var phone_val = form.phone.value;

        phone_val = phone_val.replace(RegEx2, "");

        if (form.phone.value == "" || phone_val.length != 10 || RegEx3.test(phone_val)) {
            form.phone.focus();
            form.phone.scrollIntoView(false);
            setTimeout(function () {
                alert('Некорректный номер телефона!');
            }, 500);
            console.log(RegEx3.test(phone_val), phone_val);
            form.phone.style.border = "1px solid #FF0000";
            d = 0;
        } else {
            form.phone.style.border = "1px solid #CCCCCC";
        }

        var iden_val = form.idencod.value;

        iden_val = iden_val.replace(RegEx, "");

        //определение возраста
        var birthDate = new Date('19' + iden_val.slice(0, 2), iden_val.slice(2, 4), iden_val.slice(4, 6)),
            now = new Date();

        var age = now.getFullYear() - birthDate.getFullYear();
        var m = now.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && now.getDate() < birthDate.getDate())) {
            age--;
        }
        console.log(age );
         if (age > 100 || age < 15) { //нереальнй возраст
             if (!form.inn_dis.checked) {
                 form.idencod.focus();
                 form.idencod.scrollIntoView(false);
                 setTimeout(function() {alert('Некорректный ИНН!');}, 500);
                 form.idencod.style.border = "1px solid #FF0000";
                 document.getElementById('inn_check_block').style.display = "block";
                 d = 0;
             }
         }

        form.age.value = age;
        d = 0;

        if (form.idencod.value == "" || iden_val.length != 12) {
            form.idencod.focus();
            form.idencod.style.border = "1px solid #FF0000";
            d = 0;

        } else {
            form.idencod.style.border = "1px solid #CCCCCC";
        }



        if (form.tip.value == "") {
            form.tip.focus();
            document.getElementById('tip').style.border = "1px solid #FF0000";
            d = 0;

        } else {
            document.getElementById('tip').style.border = "1px solid #CCCCCC";
        }

        if (form.summa.value == "") {
            form.summa.focus();
            form.summa.style.border = "1px solid #FF0000";
            d = 0;

        } else {
            form.summa.style.border = "1px solid #CCCCCC";
        }

        if (form.sogl.checked) {

            document.getElementById('fsogl').style.color = "#888888";

        } else {
            document.getElementById('fsogl').style.color = "#FF0000";
            d = 0;
        }

        if (d == 1) {
            $(form.form_button).attr('disabled', 'disabled');
            form.form_button.textContent = 'Отправка...';
            form.phone.value = phone_val;
            if ($('#trigger').val() != true) {
                //sendForm();
            }
        }

        if (d == 1) {
            if ($('#trigger').val() != true) {
                //setTimeout(function() {form.submit()}, 1500);
                form.submit()
            } else {
                //setTimeout(function() {form.submit()}, 1000);
                form.submit()
            }
            return false;

        }
        else {
            return false;
        }
    }

</script>

<script>

    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    jQuery(function ($) {
        $("#phone").mask("(999) 999-99-99");
        $("#code").mask("999999999999");
        if (isMobile.any()) {
            $("#device").val("Телефон");
        } else {
            $("#device").val("Компьютер");
        }
    });


</script>


