<?php defined('_JEXEC') or die;
$app = JFactory::getApplication();
$menu = $app->getMenu();
$frontPage = $menu->getActive() == $menu->getDefault();
$page   = $app->input->get('page');
include_once "getreferer.php";
$monthes = array(
	1 => 'января', 2 => 'февраля', 3 => 'марта', 4 => 'апреля',
	5 => 'мая', 6 => 'июня', 7 => 'июля', 8 => 'августа',
	9 => 'сентября', 10 => 'октября', 11 => 'ноября', 12 => 'декабря'
);
//var_dump($frontPage);
//$app = JFactory::getApplication('site');
//$template = $app->getTemplate('beez3');
//$param = $template->params->get('paramName', 'defaultValue');
?>
<!doctype html>

<html lang="<?php echo $this->language; ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Cache-Control" content="max-age=3600, must-revalidate">
    <title>Кредит 24</title>
    <link rel="icon" type="image/png" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/icon2.png" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/font-awesome-4.2.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/slick/slick.css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/fonts.css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/main.css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/media.css" />
	<? if ($page == 'order' || $page == 'order_vitrina') { ?>
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/vitrina.css" />
	<? } ?>

    <link rel="stylesheet"
          href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/jquery.fancybox.css"/>
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/jquery/jquery-3.2.1.min.js"></script>
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/slick.js"></script>
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/jquery.fancybox.js"></script>
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/inputmask/jquery.maskedinput.min.js"></script>

    <!-- Global site tag (gtag.js) - Google Ads: 703462716 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-703462716"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-703462716');
    </script>
    <script charset="UTF-8" src="//cdn.sendpulse.com/js/push/0de5d51df3e448726517faa3eda571f1_1.js" async></script>
   </head>

<body>
<div class="wrapper">
	<?php include 'header.php'; ?>
	<?php if (!$page && $frontPage){
		include 'main.php';
	}elseif ($page == 'order') {
		//if (!$_SESSION['vsi_ok']) {
			include 'order_vitrina.php';
		//} else {
		//	include 'order.php';
		//}
	}elseif ($page == 'order_vitrina') {
		include 'order_vitrina.php';
	}elseif ($page == 'confirm_page') {
		include 'confirmation_page.php';
	}else{ ?>
		<jdoc:include type="component" />
   <? } ?>

	<?php include 'footer.php'; ?>
</div>
<?php include 'modal/credit.php'; ?>
<?php include 'modal/feedback.php'; ?>
<?php include 'modal/recall.php'; ?>
<?php include 'modal/agree.php'; ?>
<?php include 'modal/documents.php'; ?>
<?php include 'modal/map.php'; ?>
<?php include 'modal/question.php'; ?>
<?php include 'modal/thank.php'; ?>


<!--[if lt IE 9]>

<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/html5shiv/es5-shim.min.js"></script>
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/html5shiv/html5shiv.min.js"></script>
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/html5shiv/html5shiv-printshiv.min.js"></script>
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/respond/respond.min.js"></script>
<![endif]-->



<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/owl-carousel/owl.carousel.min.js"></script>

<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/slick/slick.min.js"></script>

<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/common.js"></script>

</body>
</html>
