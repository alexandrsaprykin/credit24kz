<?php

// No direct access
defined( '_JEXEC' ) or die;

/**
 * Модель редактирования текущего элемента
 * @author Sapaleks
 */
class SenderModelMark extends JModelAdmin {

	/**
	 * загрузка текущей формы
	 * @param Array $data
	 * @param Boolean $loadData
	 * @return Object form data
	 */
	public function getForm( $data = array( ), $loadData = true ) {
		$form = $this->loadForm( 'com_sender.mark', 'mark', array( 'control' => 'jform', 'load_data' => $loadData ) );
		if ( empty( $form ) ) {
			return false;
		}
		$user = JFactory::getUser();
		if ( !$user->authorise( 'core.edit.state', '#__marks.' . $this->getState( 'extdataedit.id' ) ) ) {
			$form->setFieldAttribute( 'published', 'disabled', 'true' );
			$form->setFieldAttribute( 'published', 'filter', 'unset' );
		}
		return $form;
	}


	/**
	 * @param string $type
	 * @param string $prefix
	 * @param array $config
	 * @return JTable|mixed
	 */
	public function getTable( $type = 'marks', $prefix = 'Table', $config = array( ) ) {
		return JTable::getInstance( $type, $prefix, $config );
	}

	/**
	 * Загрузка данных в форму
	 * @return Object
	 */
	protected function loadFormData() {
		$data = JFactory::getApplication()->getUserState( 'com_sender.edit.mark.data', array() );
		if ( empty( $data ) ) {
			$data = $this->getItem();
		}

		return $data;
	}

	/**
	 * Запрет удаления записи
	 * @param object $record
	 * @return bool
	 */
	protected function canDelete( $record )
	{
		if ( !empty( $record->id ) ) {
			return JFactory::getUser()->authorise( 'core.delete', '#__marks.' . (int)$record->id );
		}
	}



	/**
	 * Запрет изменения состояния
	 * @param object $record
	 * @return bool
	 */
	protected function canEditState( $record )
	{
		$user = JFactory::getUser();

		// Check for existing article.
		if ( !empty( $record->id ) ) {
			return $user->authorise( 'core.edit.state', '#__marks.' . (int)$record->id );
		} // New article, so check against the category.
		elseif ( !empty( $record->catid ) ) {
			return $user->authorise( 'core.edit.state', '#__marks.' . (int)$record->catid );
		} // Default to component settings if neither article nor category known.
		else {
			return parent::canEditState( 'com_sender' );
		}
	}

	public function deleteRequestMark($request_id, $mark_id)
	{
		$db = $this->getDbo();

		$query = $this->getDbo()->getQuery(true);
		$query->delete('#__request_marks')
			->where(array('request_id = ' . $request_id, 'mark_id = ' . $mark_id));

		$db->setQuery($query);

		try {
			$db->execute();
		} catch (RuntimeException $e) {
			$this->setError($e->getMessage());
			return false;
		}

		return true;
	}

	public function addRequestsMark($request_ids, $mark_id)
	{
		$db = $this->getDbo();


		$query = $this->getDbo()->getQuery(true);
		$query->insert('#__request_marks');
		foreach ($request_ids as $request_id) {
			$query->values($request_id . ',' . $mark_id);
		}

		$db->setQuery($query);

		try {
			$db->execute();
		} catch (RuntimeException $e) {
			$this->setError($e->getMessage());
			return false;
		}

		return true;
	}

	public function deleteRequestsMark($request_ids, $mark_id)
	{
		$db = $this->getDbo();


		$query = $this->getDbo()->getQuery(true);
		$query->delete('#__request_marks');
		foreach ($request_ids as $request_id) {
			$query->where('request_id = ' . $request_id . ' AND mark_id = ' . $mark_id, 'OR');
		}

		$db->setQuery($query);

		try {
			$db->execute();
		} catch (RuntimeException $e) {
			$this->setError($e->getMessage());
			return false;
		}

		return true;
	}

}