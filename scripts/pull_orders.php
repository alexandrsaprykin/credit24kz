<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 19.12.2018
 * Time: 17:47
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
	require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
	define('JPATH_BASE', preg_replace('/(\\\|\/)scripts$/', '', dirname(__FILE__)));
	require_once JPATH_BASE . '/includes/defines.php';
}
define('JPATH_COMPONENT', JPATH_BASE . '/components/com_sender');

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

// Load the configuration
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_BASE . '/includes/framework.php';

require_once JPATH_COMPONENT . '/helpers/sender.php';

$mainframe = JFactory::getApplication('site');
$mainframe->initialise();

$db = JFactory::getDbo();

jimport('joomla.application.component.model');
jimport('joomla.application.component.helper');

JModelLegacy::addIncludePath(JPATH_COMPONENT . '/sending', 'SendingModel');

senderSiteHelper::initLogger();

$ad_model = JModelLegacy::getInstance('Admitad', 'SendingModel');
$r_model = JModelLegacy::getInstance('Request', 'SendingModel');
$com_sender = JComponentHelper::getComponent('com_sender');
$params = new JRegistry($com_sender->getParams());
$ad_id = $params->get('admitad_id');
$ad_id = 796696;
$developer_mode = $params->get('developer_mode');


$requests = $r_model->getRequests2();

foreach ($requests as $request_) {
	$save_data = [
		'date' => $request_['date'],
		'name' => $request_['name'],
		'phone' => $request_['phone'],
		'phone2' => $request_['phone2'],
		'inn' => $request_['inn'],
		'age' => $request_['age'],
		'obl' => $request_['obl'],
		'tip' => $request_['tip'],
		'duration' => $request_['duration'],
		'tip2' => $request_['tip2'],
		'sum' => $request_['sum'],
		'referer' => $request_['referer'],
		'istok' => $request_['istok'],
		'device' => $request_['device'],
		'alfa_result' => $request_['alfa_result'],
		'confirmed' => $request_['confirmed'],
		'confirm_result_log' => 1,
		'credit_type' => $request_['credit_type'],
		'email' => $request_['email'],
		'vsi_ok' => $request_['vsi_ok'],
	];

	$r_model->writeRequest($save_data);

	var_dump($request_['id']);
	echo '<br>';
	echo '---------------------------';
}


