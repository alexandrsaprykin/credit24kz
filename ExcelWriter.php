<?php
set_time_limit(-1);
ini_set('memory_limit', '-1');
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

// Initialize Joomla framework
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
	require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
	define('JPATH_BASE', dirname(__FILE__));
	require_once JPATH_BASE . '/includes/defines.php';
}
define('JPATH_COMPONENT', JPATH_BASE . '/components/com_sender');

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

// Load the configuration
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_BASE . '/includes/framework.php';

require_once JPATH_COMPONENT . '/helpers/sender.php';

$mainframe = JFactory::getApplication('site');
$mainframe->initialise();

$db = JFactory::getDbo();

jimport('joomla.application.component.helper');

JModelLegacy::addIncludePath(JPATH_COMPONENT . '/sending', 'SendingModel');

senderSiteHelper::initLogger();

$r_model = JModelLegacy::getInstance('Request', 'SendingModel');


if (isset($_POST['start']) || isset($_POST['end'])) {
	if (!$_POST['start']) {
		$_POST['start'] = time() - 86400;
	} else {
		$_POST['start'] = strtotime($_POST['start']);
	}

	if (!$_POST['end']) {
		$_POST['end'] = time();
	} else {
		$_POST['end'] = strtotime($_POST['end']) + 86400;
	}

	$result = $r_model->getRequests($_POST['start'], $_POST['end']);

	require_once(JPATH_BASE . '/templates/credit24/libs/PHPExcel.php');
// Подключаем класс для вывода данных в формате excel
	require_once(JPATH_BASE . '/templates/credit24/libs/PHPExcel/Writer/Excel5.php');

	if ($objPHPExcel = PHPExcel_IOFactory::load(JPATH_BASE . '/templates/credit24/libs/PHPExcel/templates/credit24.xlsx')) {
		$objPHPExcel->setActiveSheetIndex(0);
		$worksheet = $objPHPExcel->getActiveSheet();
		$new_row = 1;
		foreach ($result as $request) {
			$new_row = $new_row + 1;
			$worksheet->setCellValueByColumnAndRow(0, $new_row, $new_row - 1);
			$worksheet->setCellValueByColumnAndRow(1, $new_row, date("Y-m-d H:i:s", $request['date']));
			$worksheet->setCellValueByColumnAndRow(2, $new_row, $request['name']);
			$worksheet->setCellValueByColumnAndRow(3, $new_row, $request['phone']);
			$worksheet->setCellValueByColumnAndRow(4, $new_row, $request['email']);
			$worksheet->setCellValueByColumnAndRow(5, $new_row, $request['inn']);
			$worksheet->setCellValueByColumnAndRow(6, $new_row, $request['age']);
			$worksheet->setCellValueByColumnAndRow(7, $new_row, $request['obl']);
			$worksheet->setCellValueByColumnAndRow(8, $new_row, $request['tip']);
			$worksheet->setCellValueByColumnAndRow(9, $new_row, $request['duration']);
			$worksheet->setCellValueByColumnAndRow(10, $new_row, $request['sum']);
			$worksheet->setCellValueByColumnAndRow(11, $new_row, $request['referer']);
			$worksheet->setCellValueByColumnAndRow(12, $new_row, $request['istok']);
			$worksheet->setCellValueByColumnAndRow(13, $new_row, $request['device']);
			$worksheet->setCellValueByColumnAndRow(14, $new_row, $request['alfa_result']);
			$worksheet->setCellValueByColumnAndRow(15, $new_row, $request['confirm_result']);
			$worksheet->setCellValueByColumnAndRow(16, $new_row, (($request['confirmed'] == 2)? 'Не открывалось!!!!' : ''));
			$worksheet->setCellValueByColumnAndRow(17, $new_row, (($request['vsi_ok'] == '1')? '+' : ''));
		}
		ob_end_clean();
		header("Expires: 0");
		header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=credit24.xlsx");
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		$objWriter->save('php://output');

		exit;
	}

}
?>
<html>
<head>
    <link rel="stylesheet" href="libs/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"/>
</head>
<body>
<h1 style="text-align: center; margin-top: 20px">Формирование документа Excel</h1>
<form method="POST" style="margin-left: 10%; margin-top: 50px">
    <div class="form-group" style="font-size: 15px; width: 40%; ">
        <label for="inputDate" style="font-size: 25px">Выберите период даты:</label><br>
        Начало:
        <input type="date" name="start" class="form-control" style="font-size: 15px; margin-bottom: 10px">
        Конец:
        <input type="date" name="end" class="form-control" style="font-size: 15px; margin-bottom: 20px">
    </div>
    <button type="submit" class="btn btn-success" style="font-size: 20px">Сформировать документ</button>
</form>
</body>
</html>
