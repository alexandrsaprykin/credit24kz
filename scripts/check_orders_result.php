<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 19.12.2018
 * Time: 17:47
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
	require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
	define('JPATH_BASE', preg_replace('/(\\\|\/)scripts$/', '', dirname(__FILE__)));
	require_once JPATH_BASE . '/includes/defines.php';
}
define('JPATH_COMPONENT', JPATH_BASE . '/components/com_sender');

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

// Load the configuration
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_BASE . '/includes/framework.php';

require_once JPATH_COMPONENT . '/helpers/sender.php';

$mainframe = JFactory::getApplication('site');
$mainframe->initialise();

$db = JFactory::getDbo();

jimport('joomla.application.component.model');
jimport('joomla.application.component.helper');

JModelLegacy::addIncludePath(JPATH_COMPONENT . '/sending', 'SendingModel');

senderSiteHelper::initLogger();

$ad_model = JModelLegacy::getInstance('Admitad', 'SendingModel');
$com_sender = JComponentHelper::getComponent('com_sender');
$params = new JRegistry($com_sender->getParams());
$ad_id = $params->get('admitad_id');
$developer_mode = $params->get('developer_mode');

$params = array(
	'date_start' => '22.01.2019',
	'date_end' => '23.01.2019',
	'limit' => '300',
	'offset' => '3800',
);
$ad_model->start($ad_id);
$list = $ad_model->getOrderList($params);
//$list = $ad_model->getRequestResult('9988866');

foreach ($list['results'] as $key => $order){
	var_dump($order);
	echo '<br>';
	echo '---------------------------';
	echo '<br>';
}
