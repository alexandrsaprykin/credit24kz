<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 20.01.2019
 * Time: 12:54
 */
?>
<main class="content">
	<section class="credit">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="credit_title">
						<h1>Наличный кредит, кредит на карту
							<span>или</span> кредитная карта
							<span class="below_title">за 5 минут и 3 шага</span></h1>
					</div>
					<div class="credit_benefits">
						<ul>
							<li>
								<picture>
									<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/percent.png">
								</picture>Вероятность получения
								кредита - <span class="green_price">99%</span></li>
							<li>
								<picture>
									<source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/time.svg">
									<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/time.png">
								</picture> 			Процентная ставка<br/>
								<span class="red_price line-through">44%</span> <span class="green_price">12,99%*</span></li>
							<li>
								<picture>
									<source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/timer.svg">
									<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/timer.png">
								</picture>Быстрое принятие<br/>
								решения – <span class="green_price">5</span> минут</li>
							<li class="required">*совместная программа сервиса С24 и<br/>
								банков-партнеров предусматривает<br/>
								понижение ставки до 12,99%<br/>
								индивидуальную ставку для вас вы узнаете<br/> 
								после общения с представителем банка</li>
						</ul>
					</div>


					<div class="main_banner">
						<div class="main_banner_content">

							<div class="goods goods_1">
								<div class="goods_image_wrap">
									<div class="goods_border goods_border_1"></div>
									<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/goods_image_1.png" alt="">
									<div class="goods_border goods_border_2"></div>
								</div>
								<div class="goods_mobile_content goods_description_item goods_description_item_1 for_mobile">
									<p class="banner_description banner_description_mob banner_mobile ">ежемесячный* розыгрыш</p>
									<p>3х смартфонов</p>
									<p>Samsung Galaxy S9</p>
								</div>
							</div>


							<div class="banner_content">
								<a href="#credit" data-toggle="modal">  <h3 class="banner_title">ЗАПОЛНИ
										<small>заявку и</small>
										<strong>ПОЛУЧИ</strong> СТИЛЬНЫЙ СМАРТФОН<br>
										<small>или </small>НОУТБУК
									</h3>
								</a>
								<p class="banner_description">ежемесячный* розыгрыш</p>
								<div class="goods_description">
									<div class="goods_description_item goods_description_item_1">
										<p>3х смартфонов</p>
										<p>Samsung Galaxy S9</p>
									</div>
									<div class="goods_description_item goods_description_item_2">
										<p>и ноутбука Lenovo</p>
										<p>Yoga 720-15</p>
									</div>
								</div>
							</div>

							<div class="goods goods_2">
								<div class="goods_mobile_content goods_description_item goods_description_item_2 for_mobile">
									<p>и ноутбука Lenovo</p>
									<p>Yoga 720-15</p>
								</div>

								<div class="goods_image_wrap">
									<div class="goods_border goods_border_1"></div>
									<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/goods_image_2.png" alt="">
									<div class="goods_border goods_border_2"></div>
								</div>

							</div>
						</div>
						<p class="warranty">*каждого первого числа месяца</p>
					</div>

				</div>
				<div class="col-md-offset-2 col-md-5">
					<div class="credit_calc">
						<h2>Кредитный калькулятор</h2>
						<p class="request">успейте оставить заявку
							<span class="green_price">до <?php echo ((date('j', time() + 86400)).' '. $monthes[(date('n', time() + 86400))]); ?></span> 2019 – получите дополнительную <span class="green_price">скидку 5%</span>
							по ставке</p>
						<form class="calc">
							<label for="sum">Желаемая сумма кредита, тг.</label><br/>
							<input maxlength='6' type="text" id="sum" placeholder="100000"><br/>
							<label for="date">Срок кредита, мес.</label><br/>
							<select name="date" id="date">
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="18">18</option>
								<option value="24">24</option>
								<option value="36">36</option>
								<option value="48">48</option>
								<option selected value="60">60</option>
							</select><br/>
						</form>
						<h3 class="permonth">Ежемесячный платеж:</h3>
						<div class="sum">
							<div class="prices">
								<p class="september_price">Если заявку оставить<br/>
									<?php echo (date('j ') . $monthes[(date('n'))]); ?></p>
								<p class="green_sum"><span>406 </span> <sub>тг.</sub></p>
							</div>
							<div class="prices">
								<p class="september_price">Если заявку оставить<br/>
									после <?php echo (date('j ') . $monthes[(date('n'))]); ?></p>
								<p class="red_sum"><span>455 </span> <sub>тг.</sub></p>
							</div>
							<a data-fancybox data-src="#credit" href="#" id="calc_button" class="order">Оформить кредит <!--бесплатно--></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="partners">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="partners_title">
						<h2>Банки-партнеры</h2>
					</div>
					<i class="fa fa-5x fa-angle-left prev" aria-hidden="true"></i>
					<div class="partners_banks">
						<div class="banks_item">
							<a href="https://alfabank.ua/private-persons/partneri/" id="alfa-partner"><img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/alfabank.png" alt="Альфа Банк"></a>
						</div>
						<div class="banks_item">
							<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/banks/100tenge.png" alt="100tenge">
						</div>
						<div class="banks_item">
							<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/banks/money-man.png" alt="Money-man">
						</div>
                        <div class="banks_item">
							<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/banks/kredit7.png" alt="Kredit7">
						</div>
                        <div class="banks_item">
							<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/banks/tengo.png" alt="Tengo">
						</div>
                        <div class="banks_item">
							<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/banks/koke.png" alt="Koke">
						</div>
                        <div class="banks_item">
							<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/banks/gofingo.png" alt="Gofingo">
						</div>
					</div>
					<i class="fa fa-5x fa-angle-right next" aria-hidden="true"></i>
				</div>
			</div>
		</div>
	</section>
	<section class="scheme">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="scheme_title">
						<h2>Схема работы</h2>
					</div>
				</div>
			</div>
			<div class="steps">
				<div class="case">
					<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/case.png" alt="case">
					<p>Заполните онлайн-заявку<br/>
						на нашем сайте</p>
				</div>
				<div class="phone">
					<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/phone.png" alt="phone">
					<p>Отвечаете на звонок<br/>
						в течение 10 минут</p>
				</div>
				<div class="wallet">
					<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/wallet.png" alt="wallet">
					<p>Получаете кредит<br/>
                        или кредитную карту</p>
				</div>
			</div>
			<a data-fancybox data-src="#credit" href="#" class="order">Оформить кредит бесплатно</a>
		</div>
	</section>
	<section class="rules">
		<div class="container">
			<div class="row">
				<div class="col-md-offset-5 col-md-6">
					<div class="rules_title">
						<h2>Условия кредитования</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-offset-6 col-md-6">
					<div class="rules_item">
						<ul>
							<li>Сумма кредита или кредитного лимита от 5000 до 4 000 000 тг.</li>
							<li>Все регионы Казахстана</li>
							<li>Срок от 61 дня до 5 лет</li>
							<li>Ставка от 12,99%</li>
							<li>Возраст от 18 до 79 лет</li>
							<li>Максимальная годовая процентная ставка, включающая ссудный процент, а также все остальные комиссии и расходы за год - не более 39,99%</li>
							<li class="rules_hidden">Без залога и поручителей</li>
							<li class="rules_hidden">Кредит на любые цели</li>
							</ul>
						<a href="#" class="rule_show recall">Смотреть все условия</a>
					</div>
					<div class="rules_count">
						<h3>Кредитов уже оформлено:</h3>
						<p>22 145</p>
                        <a data-fancybox data-src="#credit" href="#" class="order">Оформить кредит бесплатно</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="feedback">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="feedback_title">
						<h2>Отзывы</h2>
					</div>
				</div>
			</div>
			<div>
				<div class="slides">
					<i class="fa fa-5x fa-angle-left back" aria-hidden="true"></i>
					<div class="feeds carousel">
						<div class="feed">
							<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/С24. Анастасия булгакова.jpg" alt="Отзыв">
							<h4>Анастасия Булгакова, Алматы</h4>
							<h5>менеджер по работе с клиентами</h5>
							<p>Понадобился кредит для открытия
								свобственного бизнеса, не много не мало
								1 500 000 тенге. Знакомые подсказали этот
								сайт, я зашла, ознакомилась и оформила
								кредит. Честно говоря условия меня
								поразили, очень выгодно! Рекомендую и
								вам, не пожалете. Никаких тебе процентов
								сверху, быстрое оформление.
								Рекомендую!!!</p>
						</div>
						<div class="feed">
							<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/no_photo_women.jpg" alt="Отзыв">
							<h4>Айдана Дарынбекова, Караганда</h4>
							<h5>продавец</h5>
							<p>Обратилась к компани по надобности
								кредита в размере 1 250 000 тенге.
								Забрала деньги в тот же день. Быстро,
								качественно, оперативно.
								Персональный менеджер подобрал
								наилучшие условия для меня и провел
								полную консультацию по вопросу!</p>
						</div>
						<div class="feed">
							<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/feed4.png" alt="Отзыв">
							<h4>Галина Беликова, Балхаш</h4>
							<h5>Предприниматель</h5>
							<p>Оформила кредит здесь на сайте.
								Оказалось все легко и просто ) Очень
								хорошая кредитная система и даже
								проценты устроили !!! Буду рекомендовать
								знакомым)</p>
						</div>
						<div class="feed">
							<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/feed3.png" alt="Отзыв">
							<h4>Ренат Сайфутдинов, Алматы</h4>
							<h5>Водитель</h5>
							<p>Взял кредит, хотя без доверия к банку.
								И очень удивлен, процентные ставки
								поразили. Если придется еще взять,
								конечно сюда обращусь. Очень выгодно!</p>
						</div>
						<div class="feed">
							<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/no_photo_man.jpg" alt="Отзыв">
							<h4>Бабек, Костанай</h4>
							<h5>Автомеханик</h5>
							<p>Спасибо! Тоже деньги получил)) выдали чуть меньше чем заказывал,
								пояснили что через невысокую зарплату. Но все равно спасибо)))))</p>
						</div>
						<div class="feed">
							<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/С24.2.jpg" alt="Отзыв">
							<h4>Амина Омарова, г.Алматы</h4>
							<h5>Пенсионер</h5>
							<p>Решили поставить бойлер по так как все лето в городе отключали воду.
								Наличных денег не хватило поэтому пришлось брать кредит.
								Искали в интернете отзывы про банки но наткнулись на этот сайт.
								Все вышло почти как было написано в обьявлении. Не пришлось даже никуда н идти,
								банк оказался в нашем доме)))</p>
						</div>
						<div class="feed">
							<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/С24.3.jpg" alt="Отзыв">
							<h4>Наталья, г. Шымкент</h4>
							<h5>Медсестра</h5>
							<p>Взяли кредит на детскую коляску ребенку. Получилось удобно и быстро))</p>
						</div>
						<div class="feed">
							<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/С24.4.jpg" alt="Отзыв">
							<h4>Алиан, г. Алматы</h4>
							<h5>Учитель</h5>
							<p>Взяли кредит на оплату учебы сына в уневерситете.
								Если често, то сомневались что найдется банк который даст кредит, но все получилось! </p>
						</div>
						<div class="feed">
							<img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/С24.5.jpg" alt="Отзыв">
							<h4>Елена Николаевна, г. Астана</h4>
							<h5>Пенсионер</h5>
							<p>Оформила кредитную карту на пенсию,
								Что интересно в банке где получаю пенсию  отказали. </p>
						</div>
					</div>
					<i class="fa fa-5x fa-angle-right forward" aria-hidden="true"></i>
				</div>
				<div class="col-md-12">
					<a href="#feedback" data-toggle="modal" class="give_feedback">Оставьте отзыв</a>
				</div>
			</div>
		</div>
	</section>
	<section class="faq">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="faq_title">
						<h2>Вопрос-ответ</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="accordeon">
			<dl>
				<dt>Есть ли комиссия за оформление кредитной карты или кредита?</dt>
				<dd>При оформлении кредитной карты или кредита через сервис credit24kz.com Вы не оплачиваете НИКАКИХ<br/> дополнительных комиссий.</dd>
				<div class="angle">
					<i class="fa fa-3x fa-angle-down"></i>
				</div>
			</dl>
			<dl>
				<dt>Насколько выгодно оформлять кредитную карту или кредит с помощью<br/> сервиса credit24kz.com?</dt>
				<dd>Банки-партнеры предлагают нашим клиентам кредиты под более низкие процентные ставки, так как экономят на своих издержках по оформлению и поиску клиентов.</dd>
				<div class="angle">
					<i class="fa fa-3x fa-angle-down"></i>
				</div>
			</dl>
			<dl>
				<dt>Какова вероятность что я получу кредитную карту или кредит?</dt>
				<dd>Если Вам исполнилось 18 лет, Вы официально или неофициально трудоустроены, являетесь индивидуальным пердпринимателем или пенсионером, то вероятность получения кредита почти 100%.</dd>
				<div class="angle">
					<i class="fa fa-3x fa-angle-down"></i>
				</div>
			</dl>
			<dl class="faq_hidden">
				<dt>Как быстро я получу кредитную карту или кредит?</dt>
				<dd>Большинство наших клиентов получает кредитную карту или кредит в тот же или на следующий день после подачи заявки на сайте - все зависит от Вашего желания.</dd>
				<div class="angle">
					<i class="fa fa-3x fa-angle-down"></i>
				</div>
			</dl>
			<dl class="faq_hidden">
				<dt>На какие цели я могу использовать кредитные средства?</dt>
				<dd>Целевого использования кредитных средств как такового нет – Вы можете потратить деньги на ремонт квартиры, покупку автомобиля, лечение, отдых, погашать уже существующие кредиты и т.д.</dd>
				<div class="angle">
					<i class="fa fa-3x fa-angle-down"></i>
				</div>
			</dl>
			<dl class="faq_hidden">
				<dt>Какова окончательная процентная ставка по кредиту?</dt>
				<dd>Процентная ставка по кредиту не всегда будет в том размере который указан на главной странице (12,99%) так как она зависит в том числе от банка который выдает кредит и от самого клиента (возраст, трудоустройство и т.д.)</dd>
				<div class="angle">
					<i class="fa fa-3x fa-angle-down"></i>
				</div>
			</dl><dl class="faq_hidden">
				<dt>Как минимизировать стоимость обслуживания кредита?</dt>
				<dd>Кредит, скорее всего, будет оформлен на срок до 5 лет, но в Ваших интересах погашать кредит раньше. В таком случае общая сумма переплаты будет значительно меньше, поскольку проценты насчитываются исключительно на остаток по телу кредита. Комиссия за досрочное погашение кредита отсутствует. </dd>
				<div class="angle">
					<i class="fa fa-3x fa-angle-down"></i>
				</div>
			</dl>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<a href="#" class="showAll">Смотреть все вопросы</a>
				</div>
				<div class="col-md-6">
					<div class="ask">
						<a href="#question" data-toggle="modal">Задать вопрос</a>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
