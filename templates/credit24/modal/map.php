<!-- Modal Мы на карте -->
<div class="modal" tabindex="-1" role="dialog" id="map">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Мы на карте</h4>
            </div>
            <div class="modal-body">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3455.667133785804!2d76.92906617313808!3d43.25439556972045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836dcdeb80487d%3A0x5a5910a325f1e4d4!2z0YPQu9C40YbQsCDQotC-0LvQtSDQkdC4IDgzLCDQkNC70LzQsNGC0YsgMDUwMDAwLCDQmtCw0LfQsNGF0YHRgtCw0L0!5e0!3m2!1sru!2sua!4v1569843030340!5m2!1sru!2sua" width="100%" height="420" frameborder="0" style="border:0;" allowfullscreen></iframe>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->