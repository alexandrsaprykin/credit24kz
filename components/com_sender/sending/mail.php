<?php

// No direct access
defined('_JEXEC') or die;

/**
 * Model for edit/create current element
 * @author Sapaleks
 */
class SendingModelMail extends JModelList
{
	protected $script = false;

	protected $site_name = '';

	protected $developer_mode = false;

	protected $r_model = false;

	public $aval_limit = 0;
	public $kredo_limit = 0;


	function start($script = false, $site_name = '', $developer_mode = false, $limits = 0)
	{
		$this->script = $script;
		$this->site_name = $site_name;
		$this->developer_mode = $developer_mode;
		$this->r_model = JModelLegacy::getInstance('Request', 'SendingModel');

		if (is_array($limits)) {
			foreach ($limits as $key => $limit) {
				$this->$key = $limit;
			}
		}
	}

	function sending_idea($data, $form_id = 0)
	{
		//if (!($data['age'] != "до 21 года" and $data['age'] >= 30 and $data['age'] != "больше 70" and $data['age'] <= 65 and $data['tip2'] != "VS" and
		//	$data['sum'] >= 10000 and $data['sum'] < 20000 and ($data['tip2'] == "B3m" or $data['tip2'] == "CPB1g" or $data['tip2'] == "P"))
		//) {
		if ($data['obl'] != 'Донецкая область' and $data['obl'] != 'Луганская область' and $data['sum'] > 20000) {
			require_once('lib/nusoap.php');
			$client = new nusoap_client("https://194.126.180.186:77/ScroogeCbForms.svc?wsdl", 'wsdl');
			$client->soap_defencoding = 'UTF-8';

			$params = '
                <CreateOrderBroker xmlns="http://tempuri.org/">
                <shortApp xmlns:a="http://schemas.datacontract.org/2004/07/ScroogeCbformsService" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                <a:PIB>' . $data['name'] . '</a:PIB>
                <a:agreeId>6322</a:agreeId>
                <a:desiredAgreement>I-Credit</a:desiredAgreement>
                <a:formId>' . $form_id . '</a:formId>
                <a:stateCode>' . $data['inn'] . '</a:stateCode>
                <a:telephone>' . $data['phone'] . '</a:telephone>
                <a:webMasterId>' . $data['istok'] . '</a:webMasterId>
                <a:appId>' . date("Y-m-d H:i:s", $data['date']) . '</a:appId>
                </shortApp>
                </CreateOrderBroker>
                ';

			$result = $client->call('CreateOrderBroker', $params);
			if (isset($result['CreateOrderBrokerResult']['ReturnValue']) and isset($result['CreateOrderBrokerResult']['OrderId'])) {
				$idea = 'отправлено в Ідеябанк напрямую, ответ сервера - id' . $result['CreateOrderBrokerResult']['OrderId'] . '(' . $result['CreateOrderBrokerResult']['ReturnValue'] . ')';
				//$this->to_log('отправлено в Ідеябанк напрямую, ответ сервера - id' . $result['CreateOrderBrokerResult']['OrderId'] . '(' . $result['CreateOrderBrokerResult']['ReturnValue'] . ')');
			} else {
				$idea = 'проблемы отправки в Ідеябанк напрямую, ответ сервера - ' . $result['CreateOrderBrokerResult']['ReturnValue'];
				//$this->to_log('проблемы отправки в Ідеябанк напрямую, ответ сервера - ' . $result['CreateOrderBrokerResult']['ReturnValue']);
			}

			if ($this->script) {
				$this->to_log($idea);
			}

			/*
			Результати обробки заявки
			99 – за добу заявка з ІПН вже заведена
			100 – не вірний ІПН
			101 – клієнту менше 21-го року
			102 – заборонена видачі по іншим причинам
			1 - помилка системи
			0 – заявка заведена успішно.
			*/
			//}

			return array('id' => $result['CreateOrderBrokerResult']['OrderId'], 'result' => $result['CreateOrderBrokerResult']['ReturnValue']);
		}

		return 0;
	}

	function sending_idea_new($data, $form_id = 0)
	{
		//if (!($data['age'] != "до 21 года" and $data['age'] >= 30 and $data['age'] != "больше 70" and $data['age'] <= 65 and $data['tip2'] != "VS" and
		//	$data['sum'] >= 10000 and $data['sum'] < 20000 and ($data['tip2'] == "B3m" or $data['tip2'] == "CPB1g" or $data['tip2'] == "P"))
		//) {
		//if ($data['obl'] != 'Донецкая область' and $data['obl'] != 'Луганская область' and $data['sum'] > 15000) {
		$postdata = [
			"header" => [
				"messageDate" => "30.07.2019 12:17:48",
				"messageId" => "5e06acc1-b5b5-43e6-91d6-0bc19cc18d99",
				"originator" => [
					"system" => "EXTERNAL"
				],
				"protocol" => [
					"name" => "SiteWise",
					"version" => "1.0"
				],
				"receiver" => [
					"system" => "EXTERNAL"
				],
				"responseParams" => [
					"includes" => [
						[
							"data" => "",
							"value" => false
						]
					],
					"paging" => [
						"page" => 0,
						"pageSize" => 0,
						"pagingtotalCount" => 0
					],
					"threshold" => 0
				],
				"token" => "6a8b9d55-672f-4999-9ea8-dc6848725d30"
			],
			"clientInfo" => [
				"PIB" => "Vasil Voronin",
				"city" => "Lviv",
				"email" => "fdef@mail.com",
				"mobilePhone" => "0686542458",
				"stateCode" => "3580805152"
			],
			"termin" => 21,
			"agreeId" => 6155,
			"formId" => 44,
			"summa" => "2000",
			"infoLead" => [
				"AppId" => null,
				"WebMasterId" => null
			],
			"desiredAgreement" => "sdf",
			"numberApp" => "1",
			"promocod" => "1",
			"activity" => "пенсіонер",
			"language" => 1
		];

		$postdata = json_encode($postdata);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://194.126.180.186:78/ScroogeCbForms.svc/CreateShortDeal/');
		curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: text/json;charset=UTF-8',
			'SOAPAction : "http://tempuri.org/IApplication/CreateShortDeal',
			'Content-Length: 4395',
			'Host: 10.1.33.35:2343',
			'Connection: Keep-Alive',
			'User-Agent: Apache-HttpClient/4.1.1 (java 1.5)'
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);


		//curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		$output = curl_exec($ch);
		if ($output === FALSE) {
			//Тут-то мы о ней и скажем
			echo "cURL Error: " . curl_error($ch);
			return;
		}
		print_r($output);
		curl_close($ch);
		//}


	}

	function sending_script_idea($data)
	{

		if ($data['obl'] != 'Донецкая область' and $data['obl'] != 'Луганская область' and $data['sum'] > 15000) {
			require_once('lib/nusoap.php');
			$client = new nusoap_client("https://194.126.180.186:77/ScroogeCbForms.svc?wsdl", 'wsdl');
			$client->soap_defencoding = 'UTF-8';

			$params = '
                <CreateOrderBroker xmlns="http://tempuri.org/">
                <shortApp xmlns:a="http://schemas.datacontract.org/2004/07/ScroogeCbformsService" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                <a:PIB>' . $data['name'] . '</a:PIB>
                <a:agreeId>6322</a:agreeId>
                <a:formId>93</a:formId>
                <a:desiredAgreement>I-Credit</a:desiredAgreement>
                <a:stateCode>' . $data['inn'] . '</a:stateCode>
                <a:telephone>' . $data['phone'] . '</a:telephone>
				<a:webMasterId>' . $data['istok'] . '</a:webMasterId>
                <a:appId>' . date("Y-m-d H:i:s", $data['date']) . '</a:appId>
                </shortApp>
                </CreateOrderBroker>
                ';

			$result = $client->call('CreateOrderBroker', $params);
			if (isset($result['CreateOrderBrokerResult']['ReturnValue']) and isset($result['CreateOrderBrokerResult']['OrderId'])) {
				$idea = 'отправлено в Ідеябанк, ответ сервера - id' . $result['CreateOrderBrokerResult']['OrderId'] . '(' . $result['CreateOrderBrokerResult']['ReturnValue'] . ')';
				$this->to_log('отправлено в Ідеябанк напрямую, ответ сервера - id' . $result['CreateOrderBrokerResult']['OrderId'] . '(' . $result['CreateOrderBrokerResult']['ReturnValue'] . ')');
			} else {
				$idea = 'проблемы отправки в Ідеябанк';

				$this->to_log(
					'проблемы отправки в Ідеябанк напрямую, ответ сервера - ' . $result['CreateOrderBrokerResult']['ReturnValue']
				);
			}

			/*
			Результати обробки заявки
			99 – за добу заявка з ІПН вже заведена
			100 – не вірний ІПН
			101 – клієнту менше 21-го року
			102 – заборонена видачі по іншим причинам
			1 - помилка системи
			0 – заявка заведена успішно.
			*/

			return $idea;
		}
	}


	function sending_mega($data, $mailer)
	{
		if ($data['age'] > 25 and $data['age'] != "до 21 года" and $data['tip2'] != "NeR" and $data['tip2'] != "RN" && $data['obl'] != 'Луганская область' && $data['obl'] != 'Черновицкая область' and $data['obl'] != 'Волынская область' &&
			$data['obl'] != 'Автономная Республика Крым' && $data['obl'] != 'Ровненская область' && $data['obl'] != 'Закарпатская область' && $data['obl'] != 'Одесская область'
		) {
			$mailer->ClearAllRecipients();
			$mailer->Username = 'mega.zayavka@credit24.org.ua';
			$mailer->Sender = $mailer->From = 'mega.zayavka@credit24.org.ua';
			$mailer->addRecipient('mega.zayavka@gmail.com');
			if ($mailer->Send()) {
				$this->to_log(
					'отправлено в Мегабанк'
				);
			} else {
				$this->to_log(
					'ошибка отправки в Мегабанк'
				);
			}
		}

	}

	function sending_aval($data, $mailer, $body = '')
	{
		if (!$this->aval_limit || $data['sum'] <= $this->aval_limit) {
			if ($data['age'] > 22 and $data['age'] != "до 21 года" &&
				$data['obl'] != 'Автономная Республика Крым' && $data['obl'] != 'Луганская область' and $data['obl'] != 'Донецкая область'
				//and ($data['tip'] == "Я работаю официально" or $data['tip'] == "Я получаю пенсию")
			) {
				if ($body) {
					$params = 'subid=zzv' . $data['istok'] . '&subid1=' . $data['phone'] . '&subid2=' . $data['inn'] . '&subid3=' . $data['sum'] . '&subid4=' . $data['tip2'];
					$url = 'https://ad.admitad.com/g/ny67kj1tbp053247b61b4c03ef9232/?' . $params;
					$mailer->setBody(str_replace('{{URL}}', $url, $body));
				}

				if ($this->site_name == 'Кредит24') {
					$mailer->Username = 'aval.zayavka@credit24.org.ua';
					$mailer->Sender = $mailer->From = 'aval.zayavka@credit24.org.ua';
				}

				$mailer->ClearAllRecipients();
				$mailer->addRecipient('aval.zayavka@gmail.com');
				if ($mailer->Send()) {
					$this->to_log(
						'отправлено в Авальбанк'
					);
					$this->r_model->bankSendMark(1, $data['id']);
				} else {
					$this->to_log(
						'ошибка отправки в Авальбанк'
					);
				}
			}
		} else {
			$this->to_log(
				'Отправка в Авальбанк: Превышен лимит суммы!'
			);
		}

	}

	function sending_kredo($data, $mailer, $body = '')
	{
		if (!$this->kredo_limit || $data['sum'] <= $this->kredo_limit) {
			if ($data['age'] >= 23 and $data['age'] != "до 21 года" &&
				$data['obl'] != 'Автономная Республика Крым' && $data['obl'] != 'Луганская область' and $data['obl'] != 'Донецкая область'
				and ($data['tip'] != "Я не работаю" and $data['tip'] != "Я работаю неофициально")
			) {
				if ($body) {
					$params = 'subid=zzv' . $data['istok'] . '&subid1=' . $data['phone'] . '&subid2=' . $data['inn'] . '&subid3=' . $data['sum'] . '&subid4=' . $data['tip2'];
					$url = 'https://ad.admitad.com/g/f44p7yb0m7053247b61b0e14decc16/?' . $params;
					$mailer->setBody(str_replace('{{URL}}', $url, $body));
				}

				if ($this->site_name == 'Кредит24') {
					$mailer->Username = 'kredo.zayavka@credit24.org.ua';
					$mailer->Sender = $mailer->From = 'kredo.zayavka@credit24.org.ua';
				}

				$mailer->ClearAllRecipients();
				$mailer->addRecipient('kredo.zayavka@gmail.com');
				if ($mailer->Send()) {
					$this->to_log(
						'отправлено в Кредобанк'
					);
					$this->r_model->bankSendMark(2, $data['id']);
				} else {
					$this->to_log(
						'ошибка отправки в Кредобанк'
					);
				}
			}
		} else {
			$this->to_log(
				'Отправка в Кредобанк: Превышен лимит суммы!'
			);
		}

	}

	function sending_agricol($data, $mailer)
	{

		if ($data['age'] > 23 and $data['age'] != "до 21 года" &&
			$data['obl'] != 'Автономная Республика Крым' && $data['obl'] != 'Луганская область' and $data['obl'] != 'Донецкая область'
			and ($data['tip'] == "Я работаю официально" or $data['tip'] == "Я получаю пенсию")
		) {
			if ($this->site_name == 'Кредит24') {
				$params = 'subid=zzv' . $data['istok'] . '&subid1=' . $data['phone'] . '&subid2=' . $data['inn'] . '&subid3=' . $data['sum'] . '&subid4=' . $data['tip2'];
				$url = 'https://ad.admitad.com/g/f44p7yb0m7053247b61b0e14decc16/?' . $params;
				$mailer->setBody(str_replace('{{URL}}', $url, $mailer->Body));
				$mailer->Username = 'ca.zayavka@credit24.org.ua';
				$mailer->Sender = $mailer->From = 'ca.zayavka@credit24.org.ua';
			}
			$mailer->ClearAllRecipients();
			$mailer->addRecipient('ca.zayavka@gmail.com');
			if ($mailer->Send()) {
				$this->to_log(
					'отправлено в Кредиагрікольбанк'
				);
			} else {
				$this->to_log(
					'ошибка отправки в Кредиагрікольбанк'
				);
			}
		}

	}

	function sending_cash2go($data, $mailer)
	{

		if ($data['age'] >= 23 and $data['age'] != "до 21 года" and $data['age'] != "больше 70" and $data['age'] <= 65 &&
			$data['obl'] == 'Киевская область' and (int)$data['sum'] >= 5000
		) {

			$mailer->ClearAllRecipients();
			$mailer->addRecipient('sale@cash2go.com.ua');
			if ($mailer->Send()) {
				$this->to_log(
					'отправлено в Cash2go'
				);
			} else {
				$this->to_log(
					'ошибка отправки в Cash2go'
				);
			}
		}

	}


	function sending_oksi($data, $mailer)
	{
		$mailer->ClearAllRecipients();
		$mailer->addRecipient('okcibank@ukr.net');
		if ($mailer->Send()) {
			$this->to_log(
				'отправлено в Оксибанк'
			);
		} else {
			$this->to_log(
				'ошибка отправки в Оксибанк'
			);
		}

	}

	function sending_abank($data, $mailer, $body = '')
	{
		if ($data['tip'] == "Я работаю официально" && $data['device'] == 'Телефон' && $data['age'] >= 99 and $data['age'] <= 99 and
			$data['obl'] != 'Автономная Республика Крым' && $data['obl'] != 'Луганская область' and $data['obl'] != 'Донецкая область'
		) {
			if ($body) {
				$params = 'subid=zzv' . $data['istok'] . '&subid1=' . $data['phone'] . '&subid2=' . $data['inn'] . '&subid3=' . $data['sum'] . '&subid4=' . $data['tip2'];
				$url = 'https://ad.admitad.com/g/bmi28s5dft053247b61b4e5f767a08/?' . $params;
				$mailer->setBody(str_replace('{{URL}}', $url, $body));
				$mailer->Username = 'abank.zayavka@credit24.org.ua';
				$mailer->Sender = $mailer->From = 'abank.zayavka@credit24.org.ua';
			}

			if ($this->site_name == 'Кредит24') {
				$mailer->ClearAllRecipients();
				$mailer->addRecipient('abank.zayavka@gmail.com');
			}
			if ($mailer->Send()) {
				$this->to_log(
					'отправлено в Abank'
				);
				$this->r_model->bankSendMark(3, $data['id']);
			} else {
				$this->to_log(
					'ошибка отправки в Abank'
				);
			}
		}

	}

	function sending_bpozyka($data, $mailer, $body = '')
	{
		if ($data['obl'] != 'Автономная Республика Крым' && $data['obl'] != 'Луганская область' and $data['obl'] != 'Донецкая область'
			and $data['tip'] == "Я частный предприниматель"
		) {
			if ($body) {
				$params = 'subid=zzv' . $data['istok'] . '&subid1=' . $data['phone'] . '&subid2=' . $data['inn'] . '&subid3=' . $data['sum'] . '&subid4=' . $data['tip2'];
				$url = 'https://ad.admitad.com/g/czfibcbeqf053247b61b709f3298a0/?' . $params;
				$mailer->setBody(str_replace('{{URL}}', $url, $body));
			}

			if ($this->site_name == 'Кредит24') {
				$mailer->Username = 'bpozyka.zayavka@credit24.org.ua';
				$mailer->Sender = $mailer->From = 'bpozyka.zayavka@credit24.org.ua';
			}

			$mailer->ClearAllRecipients();
			$mailer->addRecipient('bpozyka.zayavka@gmail.com');
			if ($mailer->Send()) {
				$this->to_log(
					'отправлено в Bpozyka'
				);
				$this->r_model->bankSendMark(4, $data['id']);
			} else {
				$this->to_log(
					'ошибка отправки в Bpozyka'
				);
			}
		}

	}

	function sending_otp($data, $mailer)
	{
		if ($data['email'] and $data['age'] >= 25 and $data['age'] != "до 21 года" and $data['age'] <= 67 and
			$data['obl'] != 'Автономная Республика Крым' and $data['obl'] != 'Донецкая область' and $data['obl'] != 'Луганская область' and (int)$data['sum'] >= 15000
			and $data['tip2'] != 'VS' and $data['tip2'] != 'NeR'
		) {
			$msg = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>
						<br><b>Возраст:</b> " . $data['age'] . "<br>
						<b>Тип дохода:</b> " . $data['tip'] . "<br>
						<b>E-mail:</b> " . $data['email'] . "<br>
						<b>Область фактического проживания:</b> " . $data['obl'] . "<br>
						<b>Идентификационный код:</b> " . $data['inn'] . "<br>
						<b>ФИО:</b> " . $data['name'] . "<br>
						<b>Телефон:</b> " . substr($data['phone'], 1) . "<br>";
			if ($this->site_name == 'Кредит24' || $this->site_name == 'Тутгроши') {
				$params = 'subid=zzv' . $data['istok'] . '&subid1=' . $data['phone'] . '&subid2=' . $data['inn'] . '&subid3=' . $data['sum'] . '&subid4=' . $data['tip2'];
				$url = 'https://ad.admitad.com/g/f3282ca216053247b61b591f839a6e/?' . $params;
				$msg .= "<b>Ссылка:</b> <a href=\"" . $url . "\">" . $url . "</a><br>";
			}

			if ($this->site_name == 'Кредит24') {
				$mailer->Username = 'otp.zayavka@credit24.org.ua';
				$mailer->Sender = $mailer->From = 'otp.zayavka@credit24.org.ua';
			}
			$msg .= "</body></html>";
			$mailer->setBody($msg);
			$mailer->ClearAllRecipients();
			$mailer->addRecipient('otp.zayavka@gmail.com');
			if ($mailer->Send()) {
				$this->to_log(
					'отправлено в OTP'
				);
			} else {
				$this->to_log(
					'ошибка отправки в OTP'
				);
			}
		}

	}

	function sending_cp($data, $mailer, $body = '')
	{
		if ($data['obl'] != 'Автономная Республика Крым' and $data['obl'] != 'Донецкая область' and $data['obl'] != 'Луганская область') {
			$name = explode(" ", $data['name']);
			$lastName = (isset($name[0]) ? $name[0] : $data['name']);
			$firstName = (isset($name[1]) ? $name[1] : "");
			$middleName = (isset($name[2]) ? $name[2] : "");
			$msg = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>
						<br><b>Фамилия:</b> " . $lastName . "<br>
						<b>Имя:</b> " . $firstName . "<br>
						<b>Отчество:</b> " . $middleName . "<br>
						<b>Телефон:</b> " . $data['phone'] . "<br>
						<b>Идентификационный код:</b> " . $data['inn'] . "<br>
						<b>E-mail:</b> " . $data['email'] . "<br>
						<b>Область фактического проживания:</b> " . $data['obl'] . "<br>";
			if ($this->site_name == 'Кредит24') {
				$params = 'subid=zzv' . $data['istok'] . '&subid1=' . $data['phone'] . '&subid2=' . $data['inn'] . '&subid3=' . $data['sum'] . '&subid4=' . $data['tip2'];
				$url = 'https://ad.admitad.com/g/se3myf174u053247b61b967a571c87/?' . $params;
				$msg .= "<b>Ссылка:</b> <a href=\"" . $url . "\">" . $url . "</a><br>";
				$mailer->Username = 'cp.zayavka@credit24.org.ua';
				$mailer->Sender = $mailer->From = 'cp.zayavka@credit24.org.ua';
			}
			$msg .= "</body></html>";
			$mailer->setBody($msg);
			$mailer->ClearAllRecipients();
			$mailer->addRecipient('cp.zayavka@gmail.com');
			if ($mailer->Send()) {
				$this->to_log(
					'отправлено в CP'
				);
			} else {
				$this->to_log(
					'ошибка отправки в CP'
				);
			}
		}

	}

	function sending_finline($data, $channel = '')
	{
		$err = false;

		$partner = "2829";
		$productType = "creditCash";

		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		$phoneOperator = array("039", "050", "063", "066", "067", "068", "091", "092", "093", "094", "095", "096", "097", "098", "099", "073");

		$phone = $data['phone'];

		$phoneCode = substr($data['phone'], 0, 3);

		if (in_array($phoneCode, $phoneOperator)) {
			$phone = "38" . $phone;
		} else {
			$phone = "ErrorCode:" . $phone;
			$err = true;
		}

		$name = explode(" ", $data['name']);
		$lastName = (isset($name[0]) ? $name[0] : $data['name']);
		$firstName = (isset($name[1]) ? $name[1] : "");
		$middleName = (isset($name[2]) ? $name[2] : "");

		$age = strip_tags(($data['age']));

		$amount = $data['sum'];
		$amount = str_replace(' ', '', $amount);

		if (strpos($amount, '-')) $amount = substr($amount, 0, strpos($amount, '-'));
		if (strpos($amount, '.')) $amount = substr($amount, 0, strpos($amount, '.'));

		$employment = "";
		if ($data['tip'] == "Я не работаю") $employment = 'no';
		if ($data['tip'] == "Я частный предприниматель") $employment = 'private';
		if ($data['tip'] == "Я получаю пенсию") $employment = 'pensioner';
		if ($data['tip'] == "Я работаю официально") $employment = 'official';
		if ($data['tip'] == "Я работаю неофициально") $employment = 'unofficial';

		$aim = "renovation";
		$city = (($data['obl']));
		$identCode = strip_tags(($data['inn']));
		$istok = $data['istok'];
		$time = date("Y-m-d H:i:s");

		$data2 = array("partner" => urlencode($partner),
			"productType" => urlencode($productType),
			"ip" => urlencode($ip),
			"phone" => urlencode($phone),
			"firstName" => ($firstName),
			"lastName" => ($lastName),
			"middleName" => ($middleName),
			"age" => urlencode($age),
			"amount" => urlencode($amount),
			"employment" => urlencode($employment),
			"aim" => urlencode($aim),
			"city" => ($city),
			"channel" => urlencode($channel),
			"identCode" => urlencode($identCode),
			"url" => urlencode($istok),
			//"url" => $this->site_name,
			"agree" => true,
			"agreeTime" => $time,
		);

		$urlApi = 'http://partner.finline.ua/api/apply/';
		$request = $urlApi . "?" . http_build_query($data2);
		$curl = curl_init();

		if ($curl) {
			curl_setopt($curl, CURLOPT_URL, $request);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$out = curl_exec($curl);
			curl_close($curl);
		}
		$out = json_decode($out, true);

		if ($out['result']) {
			$finline = 'Отправлено в Finline. Статус: ' . ($out['isDeclined'] ? 'Отклонена. Причина: ' . $out['declineReason'] : 'Принята');
			$this->to_log(
				'Отправлено в Finline. Статус: ' . ($out['isDeclined'] ? 'Отклонена. Причина: ' . $out['declineReason'] : 'Принята')
			);
		} else {
			$finline = 'Не отправлено в Finline. Ошибка: ' . $out['error'] . ' ' . $out['errorMessage'];
			$this->to_log(
				'Не отправлено в Finline. Ошибка: ' . $out['error'] . ' ' . $out['errorMessage']
			);
		}
		return $finline;

	}

	function sending_script_finline($data)
	{
		$err = false;

		$partner = "2829";
		$productType = "creditCash";
		$ip = "";
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		$phoneOperator = array("039", "050", "063", "066", "067", "068", "091", "092", "093", "094", "095", "096", "097", "098", "099", "073");

		$phone = $data['phone'];

		$phoneCode = substr($data['phone'], 0, 3);

		if (in_array($phoneCode, $phoneOperator)) {
			$phone = "38" . $phone;
		} else {
			$phone = "ErrorCode:" . $phone;
			$err = true;
		}

		$name = explode(" ", $data['name']);
		$lastName = (isset($name[0]) ? $name[0] : $data['name']);
		$firstName = (isset($name[1]) ? $name[1] : "");
		$middleName = (isset($name[2]) ? $name[2] : "");
		$age = strip_tags(($data['age']));
		$amount = $data['sum'];
		$amount = str_replace(' ', '', $amount);

		if (strpos($amount, '-')) $amount = substr($amount, 0, strpos($amount, '-'));
		if (strpos($amount, '.')) $amount = substr($amount, 0, strpos($amount, '.'));

		$employment = "";
		if ($data['tip'] == "Я не работаю") $employment = 'no';
		if ($data['tip'] == "Я частный предприниматель") $employment = 'private';
		if ($data['tip'] == "Я получаю пенсию") $employment = 'pensioner';
		if ($data['tip'] == "Я работаю официально") $employment = 'official';
		if ($data['tip'] == "Я работаю неофициально") $employment = 'unofficial';

		$aim = "renovation";
		$city = (($data['obl']));
		$channel = "K24";
		$identCode = strip_tags(($data['inn']));
		$sinkCb = "http://credit24.org.ua";
		$declineCb = "http://credit24.org.ua";
		$istok = $data['istok'];
		$time = date("Y-m-d H:i:s");

		$data2 = array("partner" => urlencode($partner),
			"productType" => urlencode($productType),
			"ip" => urlencode($ip),
			"phone" => urlencode($phone),
			"firstName" => ($firstName),
			"lastName" => ($lastName),
			"middleName" => ($middleName),
			"age" => urlencode($age),
			"amount" => urlencode($amount),
			"employment" => urlencode($employment),
			"aim" => urlencode($aim),
			"city" => ($city),
			"channel" => urlencode($channel),
			"identCode" => urlencode($identCode),
			"url" => urlencode($istok),
			//"url" => $this->site_name,
			"agree" => true,
			"agreeTime" => $time,
		);

		$urlApi = 'http://partner.finline.ua/api/apply/';
		$request = $urlApi . "?" . http_build_query($data2);

		$curl = curl_init();

		if ($curl) {
			curl_setopt($curl, CURLOPT_URL, $request);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$out = curl_exec($curl);
			curl_close($curl);
		}
		$out = json_decode($out, true);

		if ($out['result']) {
			$finline = 'Отправлено в Finline. Статус: ' . ($out['isDeclined'] ? 'Отклонена. Причина: ' . $out['declineReason'] : 'Принята');
			$this->to_log(
				'Отправлено в Finline. Статус: ' . ($out['isDeclined'] ? 'Отклонена. Причина: ' . $out['declineReason'] : 'Принята')
			);
		} else {
			$finline = 'Не отправлено в Finline. Ошибка: ' . $out['error'] . ' ' . $out['errorMessage'];
			$this->to_log('Не отправлено в Finline. Ошибка: ' . $out['error'] . ' ' . $out['errorMessage']);
		}

		return $finline;
	}

	public function sending_cash2go_api($data, $login, $pass)
	{
		if ($data['age'] >= 23 and $data['age'] != "до 21 года" and $data['age'] != "больше 70" and $data['age'] <= 65 &&
			($data['obl'] == 'Киевская область' or $data['obl'] == 'Волынская область' or $data['obl'] == 'Черновицкая область'
				or $data['obl'] == 'Закарпатская область' or $data['obl'] == 'Житомирская область' or $data['obl'] == 'Черкасская область'
				or $data['obl'] == 'Кировоградская область' or $data['obl'] == 'Донецкая область' or $data['obl'] == 'Луганская область')
			and (int)$data['sum'] >= 5000
		) {
			if ($this->developer_mode) {
				$url = 'http://server.qwerty.org.ua/Cash2goTest/hs/api/loanRequests/loanRequest';
				// $url = 'http://c.cash2go.com.ua/1C/hs/api/loanRequests/';
			} else {
				$url = 'http://c.cash2go.com.ua/1C/hs/api/loanRequests/loanRequest';
			}

			$name = explode(" ", $data['name']);
			$firstName = (isset($name[0]) ? $name[0] : $data['name']);
			$lastName = (isset($name[1]) ? $name[1] : null);
			$middleName = (isset($name[2]) ? $name[2] : null);
			$phone = substr_replace($data['phone'], "-", 6, 0);
			$phone = substr_replace($phone, "-", 9, 0);
			$phone = substr_replace("+38(" . $phone, ")", 7, 0);

			switch ($data['obl']) {
				case "Киевская область":
					$region = 2;
					break;
				case "Винницкая область":
					$region = 3;
					break;
				case "Волынская область":
					$region = 4;
					break;
				case "Днепропетровская область":
					$region = 5;
					break;
				case "Донецкая область":
					$region = 6;
					break;
				case "Житомирская область":
					$region = 7;
					break;
				case "Закарпатская область":
					$region = 8;
					break;
				case "Запорожская область":
					$region = 9;
					break;
				case "Ивано-Франковская область":
					$region = 10;
					break;
				case "Кировоградская область":
					$region = 11;
					break;
				case "Луганская область":
					$region = 12;
					break;
				case "Львовская область":
					$region = 13;
					break;
				case "Николаевская область":
					$region = 14;
					break;
				case "Одесская область":
					$region = 15;
					break;
				case "Полтавская область":
					$region = 16;
					break;
				case "Ровненская область":
					$region = 17;
					break;
				case "Сумская область":
					$region = 18;
					break;
				case "Тернопольская область":
					$region = 19;
					break;
				case "Харьковская область":
					$region = 20;
					break;
				case "Херсонская область":
					$region = 21;
					break;
				case "Хмельницкая область":
					$region = 22;
					break;
				case "Черкасская область":
					$region = 23;
					break;
				case "Черновицкая область":
					$region = 24;
					break;
				case "Черниговская область":
					$region = 25;
					break;
				case "Автономная Республика Крым":
					$region = 26;
					break;
				default:
					$region = 1;
					break;
			}

			$amount = $data['sum'];
			$amount = str_replace(' ', '', $amount);

			$employment = 0;
			if ($data['tip'] == "Я не работаю") $employment = 2;
			if ($data['tip'] == "Я частный предприниматель") $employment = 8;
			if ($data['tip'] == "Я получаю пенсию") $employment = 3;
			if ($data['tip'] == "Я работаю официально") $employment = 7;
			if ($data['tip'] == "Я работаю неофициально") $employment = 1;

			$data2 = [
				'Name' => $firstName,
				'Surname' => $lastName,
				'Fname' => $middleName,
				'webMasterId' => null,
				'appId' => null,
				'IP' => null,
				'GA' => null,
				'GUI' => null,
				'ProdId' => null,
				'Term' => null,
				'Goal' => null,
				'stateCode' => $data['inn'],
				'telephone' => $phone,
				'Region' => $region,
				'Amount' => $amount,
				'employment' => $employment,
			];

			$post = json_encode($data2, JSON_UNESCAPED_UNICODE);

			$b64 = base64_encode($login . ':' . $pass);
			$headers = [
				'Authorization: Basic ' . $b64,
				'Content-Type: application/json;charset=utf-8'
			];
			//var_dump($data2, $post);
			//var_dump($headers);
			$process = curl_init($url);
			curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($process, CURLOPT_TIMEOUT, 30);
			curl_setopt($process, CURLOPT_POST, 1);
			curl_setopt($process, CURLOPT_POSTFIELDS, $post);
			curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
			$return = curl_exec($process);
			if ($return === false) {
				throw new Exception(curl_error($process), curl_errno($process));
			}
			curl_close($process);

			$return = json_decode($return, 1);

			$result = '';
			if (is_array($return)) {
				$result = 'Запрос №' . $return['reqid'] . ' Ответ: ' . $return['msg'] . ' ' . $return['state'];
			} else {
				$result = 'Неизвестная ошибка!!!';
			}
			var_dump($return);
			$this->to_log('Отправка в Cash2go: ' . $result);

			return $return;
		}
		return 0;
	}

    function sending_alfa($data, $mailer, $body)
    {
        if ($data['age'] >= 21 and $data['age'] != "до 21 года" and
            (int)$data['sum'] >= 15000 and $data['tip2'] != 'RN' and $data['tip2'] != 'NeR' and
            $this->site_name == 'Кредит24Kz'
        ) {
            $params = 'subid=zzv' . $data['istok'] . '&subid1=' . $data['phone'] . '&subid2=' . $data['inn'] . '&subid3=' . $data['sum'] . '&subid4=' . $data['tip2'];
            $url = 'https://ad.admitad.com/g/fuhffgzzsg68ad9a6dba6bad6591b4/?' . $params;
            $mailer->setBody(str_replace('{{URL}}', $url, $body));
            $mailer->ClearAllRecipients();
            $mailer->addRecipient('alfakz.zayavka@gmail.com');
            if ($mailer->Send()) {
                $this->to_log(
                    'отправлено в Альфа-банк'
                );
            } else {
                $this->to_log(
                    'ошибка отправки в Альфа-банк'
                );
            }
        }
    }

    function sending_kazkreditline($data, $mailer, $body)
    {
        if ($data['age'] >= 21 and $data['age'] != "до 21 года" and
            (int)$data['sum'] >= 15000 and $data['tip2'] != 'RN' and $data['tip2'] != 'NeR' and
            $this->site_name == 'Кредит24Kz'
        ) {
            $params = 'subid=zzv' . $data['istok'] . '&subid1=' . $data['phone'] . '&subid2=' . $data['inn'] . '&subid3=' . $data['sum'] . '&subid4=' . $data['tip2'];
            $url = 'https://ad.admitad.com/g/2g12o9ohpv68ad9a6dbae98ed5a428/?' . $params;
            $mailer->setBody(str_replace('{{URL}}', $url, $body));
            $mailer->ClearAllRecipients();
            $mailer->addRecipient('kazcredit.zayavka@gmail.com');
            if ($mailer->Send()) {
                $this->to_log(
                    'отправлено в KazCreditLine'
                );
            } else {
                $this->to_log(
                    'ошибка отправки в KazCreditLine'
                );
            }
        }
    }

    function sending_homekredit($data, $mailer, $body)
    {
        if ($data['age'] >= 23 and
            $this->site_name == 'Кредит24Kz'
        ) {
            $params = 'subid=zzv' . $data['istok'] . '&subid1=' . $data['phone'] . '&subid2=' . $data['inn'] . '&subid3=' . $data['sum'] . '&subid4=' . $data['tip2'];
            $url = 'https://ad.admitad.com/g/dyb7ymjzel68ad9a6dba8bfc45ecfd/?' . $params;
            $mailer->setBody(str_replace('{{URL}}', $url, $body));
            $mailer->ClearAllRecipients();
            $mailer->addRecipient('hckz.zayavka@gmail.com');
            if ($mailer->Send()) {
                $this->to_log(
                    'отправлено в HomeCredit'
                );
            } else {
                $this->to_log(
                    'ошибка отправки в HomeCredit'
                );
            }
        }
    }

    function sending_allkz($data, $mailer, $msg)
    {
        if ($this->site_name == 'Кредит24Kz') {
            $mailer->setBody($msg);
            $mailer->ClearAllRecipients();
            $mailer->addRecipient('credit24kz.zayavka@gmail.com');
            if ($mailer->Send()) {
                $this->to_log(
                    'отправлено в credit24kz.zayavka'
                );
            } else {
                $this->to_log(
                    'ошибка отправки в credit24kz.zayavka'
                );
            }
        }
    }

	public function to_log($str)
	{
		if ($this->script) {
			JLog::add($str, \JLog::DEBUG, 'script_sender');
		} else {
			JLog::add($str, \JLog::DEBUG, 'com_sender');
		}

		return false;
	}


}