<?php

// No direct access
defined('_JEXEC') or die;

/**
 * View to display a list of items
 * @author Sapaleks
 */
class SenderViewRequests extends JViewLegacy
{
	/**
	 * @var $items stdClass[]
	 */
	public $items;
	/**
	 * @var $total integer
	 */
	public $total;
	/**
	 * @var $items stdClass[]
	 */
	public $marks;
	/**
	 * @var $items stdClass[]
	 */
	public $banks;
	/**
	 * @var $pagination JPagination
	 */
	public $pagination;
	/**
	 * @var $state JObject
	 */
	public $state;
	/**
	 * @var $user JUser
	 */
	public $user;
	/**
	 * @var $authors stdClass[]
	 */
	public $authors;
	/**
	 * @var $authors stdClass[]
	 */
	public $filterForm;

	/**
	 * Method to display the current pattern
	 * @param type $tpl
	 */
	public function display($tpl = null)
	{
		$this->items = $this->get('Items');
		$this->total = $this->get('Total');
		$this->marks = $this->get('Marks');
		$this->banks = $this->get('Banks');
		$this->pagination = $this->get('Pagination');
		$this->state = $this->get('State');
		//$this->authors = $this->get( 'Authors' );
		$this->user = JFactory::getUser();
		$this->filterForm = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		//var_dump($this->filterForm);
		$this->loadHelper('sender');
		if ($this->getLayout() !== 'modal') {
			$this->addToolbar();
			senderHelper::addSubmenu( 'requests' );
			$this->sidebar = JHtmlSidebar::render();
		}
		parent::display($tpl);
	}

	/**
	 * Method to display the toolbar
	 */
	protected function addToolbar()
	{
		JToolBarHelper::title(JText::_('COM_SENDER'));
		$canDo = senderHelper::getActions('request');

		if (($canDo->get('core.edit')) || ($canDo->get('core.edit.own'))) {
			//JToolBarHelper::editList('request.edit');
		}

		if ($canDo->get('core.edit.state')) {
			if ($canDo->get('core.delete')) {
				//JToolBarHelper::deleteList('DELETE_QUERY_STRING', 'requests.delete', 'JTOOLBAR_DELETE');
				//JToolBarHelper::divider();
			}

			if ($canDo->get('core.admin')) {
				JToolBarHelper::preferences('com_sender');
				JToolBarHelper::divider();
			}
			JToolBarHelper::custom('', 'redo', '124', 'Отправить через скрипт', false);
		}
	}

	protected function getSortFields()
	{
		return array(
			'ordering' => JText::_( 'JGRID_HEADING_ORDERING' ),
			'id' => 'ID',
			'date' => 'Дата',
			'name' => 'ФИО',
			'phone' => 'Телефон',
			'inn' => 'ИНН',
			'admitad' => 'Адмитад',
		);
	}
}