<?php

// No direct access
defined( '_JEXEC' ) or die;

/**
 * @author Sapaleks
 */
class SenderModelCampaigns extends JModelList
{

	/**
	 * Конструктор класса
	 * @param Array $config
	 */
	public function __construct( $config = array() )
	{
		if ( empty( $config['filter_fields'] ) ) {
			$config['filter_fields'] = array( 'title', 'state', 'ordering', 'created_by', 'created', 'id', 'author_id' );
		}
		parent::__construct( $config );
	}

	/**
	 * @param String $ordering
	 * @param String $direction
	 */
	protected function populateState( $ordering = null, $direction = null )
	{
		if ( $layout = JFactory::getApplication()->input->get( 'layout' ) ) {
			$this->context .= '.' . $layout;
		}
		$search = $this->getUserStateFromRequest( $this->context . '.filter.search', 'filter_search' );
		$this->setState( 'filter.search', $search );
		$published = $this->getUserStateFromRequest( $this->context . '.filter.published', 'filter_published', '' );
		$this->setState( 'filter.published', $published );
		$authorId = $this->getUserStateFromRequest( $this->context . '.filter.author_id', 'filter_author_id' );
		$this->setState( 'filter.author_id', $authorId );
		parent::populateState( 'id', 'desc' );
	}

	/**
	 * @param string $id
	 * @return string
	 */
	protected function getStoreId( $id = '' )
	{
		$id .= ':' . $this->getState( 'filter.search' );
		$id .= ':' . $this->getState( 'filter.published' );
		$id .= ':' . $this->getState( 'filter.author_id' );
		return parent::getStoreId( $id );
	}

	/**
	 * Составление запроса для получения списка записей
	 * @return JDatabaseQuery
	 */
	protected function getListQuery()
	{
		$query = $this->getDbo()->getQuery( true );
		$query->select( 't1.id, t1.campaign_id, t1.alias, t1.active' );
		$query->from( '#__admitad_campaigns as t1' );

		$orderCol = $this->state->get( 'list.ordering' );
		$orderDirn = $this->state->get( 'list.direction' );
		$query->order( $this->getDbo()->escape( $orderCol . ' ' . $orderDirn ) );
		return $query;
	}

}