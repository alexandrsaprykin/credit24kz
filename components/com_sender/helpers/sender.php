<?php

// No direct access
defined('_JEXEC') or die;

/**
 * Component helper
 * @author Sapaleks
 */
class SenderSiteHelper
{
	static function initLogger()
	{
		$options = array('text_entry_format' => "{MESSAGE} - {DATE} {TIME}\r", 'text_file' => 'sending.log', 'text_file_path' => \JFactory::getConfig()->get('site_path'));
		JLog::addLogger($options, JLog::ALL, array('com_sender'));
	}

	static function initLoggerScript()
	{
		$options = array('text_entry_format' => "{MESSAGE} - {DATE} {TIME}\r", 'text_file' => 'sending.log', 'text_file_path' => \JFactory::getConfig()->get('site_path') . '/separate_form_script_fl_idea');
		JLog::addLogger($options, JLog::ALL, array('script_sender'));
	}

	static function initLoggerInfo()
	{
		$options = array('text_entry_format' => "{MESSAGE} - {DATE} {TIME}\r", 'text_file' => 'log.txt', 'text_file_path' => \JFactory::getConfig()->get('site_path'));
		JLog::addLogger($options, JLog::ALL, array('com_sender'));
	}

	static function initLoggerDeclined()
	{
		$options = array('text_entry_format' => "{MESSAGE} - {DATE} {TIME}\r", 'text_file' => 'declined.txt', 'text_file_path' => \JFactory::getConfig()->get('site_path'));
		JLog::addLogger($options, JLog::ALL, array('com_sender_declined'));
	}

	static function initLoggerSms()
	{
		$options = array('text_entry_format' => "{MESSAGE}\r", 'text_file' => 'sms.txt', 'text_file_path' => \JFactory::getConfig()->get('site_path'));
		JLog::addLogger($options, JLog::ALL, array('com_sender_sms'));
	}

	/**
	 * @var array $menuIds List Id depending of view component
	 */
	static $menuIds = array();

	/**
	 * Create sef links
	 * @param $option string
	 * @param $view string
	 * @param $query string
	 * @return string link
	 * @throws Exception
	 */
	static function getRoute($option, $view, $query = '')
	{
		if (empty(self::$menuIds[$option . '.' . $view])) {
			$items = JMenuSite::getInstance('site')->getItems('component', $option);
			foreach ($items as $item) {
				if (isset($item->query['view']) && $item->query['view'] === $view) {
					self::$menuIds[$option . '.' . $view] = $item->id;
				}
			}
		}
		return JRoute::_('index.php?view=' . $view . $query . '&Itemid=' . self::$menuIds[$option . '.' . $view]);
	}

	/**
	 * set meta tags
	 * @param string $title
	 * @param string $metaDesc
	 * @param string $metaKey
	 * @throws Exception
	 */
	static function setDocument($title = '', $metaDesc = '', $metaKey = '')
	{
		$baseUrl = JUri::base();
		$doc = JFactory::getDocument();
		$doc->addScript($baseUrl . 'components/com_sender/assets/scripts/test.js')
			->addStyleSheet($baseUrl . 'components/com_sender/assets/styles/test.css');
		$app = JFactory::getApplication();
		if (empty($title)) {
			$title = $app->get('sitename');
		} elseif ($app->get('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		} elseif ($app->get('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}
		$doc->setTitle($title);
		if (trim($metaDesc)) {
			$doc->setDescription($metaDesc);
		}
		if (trim($metaKey)) {
			$doc->setMetaData('keywords', $metaKey);
		}
	}

	static function admitadAlfaSend($data, $alfa_id, $cash = false)
	{

		//нереальный возраст
		$data['age2'] = 0;
		if ($data['age'] > 100 || $data['age'] < 15) {
			$data['age2'] = $data['age'];
			$data['age'] = 40;
		}


		$name = explode(" ", $data['contact_name']);
		$lastName = (isset($name[0]) ? $name[0] : $data['contact_name']);
		$firstName = (isset($name[1]) ? $name[1] : "");
		$middleName = (isset($name[2]) ? $name[2] : "");
		$fio = $data['contact_name'];
		$phone = $data['phone'];
		$inn = $data['idencod'];
		$sum = $data['summa'];
		if (strpos($sum, '-')) $sum = substr($sum, 0, strpos($sum, '-'));
		if (strpos($sum, '.')) $sum = substr($sum, 0, strpos($sum, '.'));
		if ($cash) {
			if ($data['tip'] == "Я работаю неофициально") $trud = 5;
			if ($data['tip'] == "Я не работаю") $trud = 4;
			if ($data['tip'] == "Я частный предприниматель") $trud = 3;
			if ($data['tip'] == "Я получаю пенсию") $trud = 2;
			if ($data['tip'] == "Я работаю официально") $trud = 6;
			if ($data['tip'] == "Я военнослужащий") $trud = 6;
		} else {
			if ($data['tip'] == "Я работаю неофициально") $trud = 9;
			if ($data['tip'] == "Я не работаю") $trud = 3;
			if ($data['tip'] == "Я частный предприниматель") $trud = 1;
			if ($data['tip'] == "Я получаю пенсию") $trud = 4;
			if ($data['tip'] == "Я работаю официально") $trud = 5;
			if ($data['tip'] == "Я военнослужащий") $trud = 5;
		}

		$data['istok'] = str_replace('-', '', $data['istok']);
		$partner = 'https://ad.admitad.com/g/' . $alfa_id . '/?subid=' . $data['istok'] . '&subid1=' . $phone . '&subid2=' . $inn . '&subid3=' . $sum . '&subid4=' . $data['tip2'];

		if ($cash) {
			$urlApi = 'https://alfa-credits.com.ua/submit/';
			//5351d3d56e053247b61be6dcee139a74f9aacd8f
		} else {
			$urlApi = 'https://alfa-credits.com.ua/card/submit/';
			//5351d3d56e053247b61be6dcee139a0ef8bffa25
		}


		$send_data = array(
			'name' => $fio,
			"first_name" => ($firstName),
			"last_name" => ($lastName),
			"inn" => $inn,
			"phone" => $phone,
			"placement" => $trud,
			"url" => $partner,
			"confirmation" => 1,
		);

		$request = $urlApi . "?" . http_build_query($send_data);
//var_dump($request);
		/* $curl = curl_init();

		 if ($curl) {
			 curl_setopt($curl, CURLOPT_URL, $request);
			 curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			 $out = curl_exec($curl);
			 curl_close($curl);
		 }
		 $out = json_decode($out, true);*/
		$out = json_decode(file_get_contents($request), true);

		$data['sum'] = $data['summa'];
		$data['alfa_result'] = $out['status'];
		$vsi_ok = self::getVsiOkCondition($data);

		$type = $cash ? '"КЕШ"' : '"Карта"';
		//$out['status'] = 'sended sms';
		if ($out['status'] == 'sended sms') {
			if ($data['obl'] == 'Киевская область' && is_numeric($data['summa']) && $data['summa'] >= 50000 && $data['summa'] <= 100000 and $vsi_ok and $data['tip2'] != "CPm1g" and $data['tip2'] != "CPB1g"/* and $data['obl'] != 'Черновицкая область' and $data['obl'] != 'Автономная Республика Крым' and
				$data['obl'] != 'Луганская область' and $data['obl'] != 'Донецкая область' and $data['age'] != "до 21 года" and $data['age'] >= 21*/
			) {
				$result_text = 'Успешно. Окно для подтверждения смс не открылось по условиям!!! ' . $type;
				$out['status'] = 'non_confirmed';
			} else {
				//to_log('отправка в Адмитад-Альфа. Заявка от ' . $data['contact_name'] . '. Результат: Успешно. Ожидает подтверждения по смс');
				$result = 'Успешно';
				$result_text = 'Успешно. Ожидает подтверждения по смс ' . $type;
			}
		} elseif ($out['status'] == 'error') {
			if (!is_array($out['error']) && stristr($out['error'], "It took less than 30 days")) {
				//to_log('отправка в Адмитад-Альфа. Заявка от ' . $data['contact_name'] . '. Результат: Дубль(' .$out['error'] . ')');
				$result = 'Дубль';
				$result_text = 'Дубль(' . $out['error'] . ') ' . $type;
			} else {
				//to_log('отправка в Адмитад-Альфа. Заявка от ' . $data['contact_name'] . '. Результат: Ошибка. Текст: ' . (is_array($out['error'])? urldecode(http_build_query($out['error'])) : $out['error']));
				$result = 'Ошибка: ' . (is_array($out['error']) ? urldecode(http_build_query($out['error'])) : $out['error']);
				$result_text = 'Ошибка: ' . (is_array($out['error']) ? urldecode(http_build_query($out['error'])) : $out['error']) . ' ' . $type;
			}
		} elseif ($out['status'] == 'success') {
			//to_log('отправка в Адмитад-Альфа. Заявка от ' . $data['contact_name'] . '. Результат: Отказано (банк не принял заявку)');
			$result = 'Отказано';
			$result_text = 'Отказано (банк не принял заявку) ' . $type;
		} else {
			//to_log('отправка в Адмитад-Альфа. Заявка от ' . $data['contact_name'] . '. Результат: неизвестный результат: ' . $out['status']);
			$result = 'Неизвестный результат: ' . $out['status'];
			$result_text = 'Неизвестный результат: ' . $out['status'] . ' ' . $type;
		}

		$str = json_encode($send_data, JSON_UNESCAPED_UNICODE) . ' ---- ' . json_encode($out);
		self::initLoggerInfo();
		JLog::add($str, \JLog::DEBUG, 'com_sender');
		// }else to_log('Не отправлено в Адмитад-Альфа (сумма больше 1 000 000)');
		return array('out' => $out, 'result' => $result, 'result_text' => $result_text, 'vsi_ok' => $vsi_ok);
	}

	static function getVsiOkCondition($data)
	{
		$vsi_ok = true;
		if ($data['obl'] != 'Автономная Республика Крым' and $data['obl'] != 'Луганская область' and $data['obl'] != 'Донецкая область' and $data['obl'] != 'Волынская область' and $data['obl'] != 'Ровненская область' and $data['obl'] != 'Закарпатская область' and $data['obl'] != 'Тернопольская область' and $data['obl'] != 'Хмельницкая область' and $data['obl'] != 'Черновицкая область' and $data['obl'] != 'Винницкая область' and $data['obl'] != 'Сумская область' and $data['obl'] != 'Черниговская область' and $data['obl'] != 'Черкасская область' and $data['obl'] != 'Кировоградская область'and $data['obl'] != 'Житомирская область' and $data['obl'] != 'Запорожская область' and $data['obl'] != 'Николаевская область' and $data['obl'] != 'Херсонская область') {
			if (($data['tip2'] == "NeR" or $data['tip2'] == "RN" or $data['tip2'] == "VS" or $data['tip2'] == "m3m" or $data['tip2'] == "CPm1g")) {
				$vsi_ok = false;
			} else {
				if (stristr($data['alfa_result'], "declined") or stristr($data['alfa_result'], "Дубль")) {
					if ($data['age'] >= 34 and $data['age'] != "до 21 года" and (!is_numeric($data['sum']) || $data['sum'] >= 15000)) {//условия передачи при ответе "отказано"
						$vsi_ok = true;
					} else {
						$vsi_ok = false;
					}
				} else {
					if ($data['age'] >= 25 and $data['age'] != "до 21 года" and (!is_numeric($data['sum']) || $data['sum'] >= 15000)) {//условия передачи при ответе "Успешно" и др.
						$vsi_ok = true;
					} else {
						$vsi_ok = false;
					}
				}
			}
		} else {
			$vsi_ok = false;
		}

		return $vsi_ok;
	}
}