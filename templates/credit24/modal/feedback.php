<!-- Оставить Отзыв -->
<div class="modal" tabindex="-1" role="dialog" id="feedback">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Оставить отзыв</h4>
            </div>
            <div class="modal-body">
                <form class="second_form">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" value="Отзыв" data-name = "type" class="for-ajax-feedback">
                            <label for="full_name">Ваше Имя и Фамилия</label><br/>
                            <input type="text" name="name" id="full_name" placeholder="Ваше Имя и Фамилия" data-name ="Имя" class="for-ajax-feedback" required><br/>
                            <label for="address">Город проживания</label><br/>
                            <input type="text" class="for-ajax-feedback" data-name ="Город" id="address" name="address" required placeholder="Алматы">
                        </div>
                        <div class="col-md-6">
                            <label for="prof">Ваша должность</label><br/>
                            <input type="text" class="for-ajax-feedback"  data-name ="Должность" name="prof" id="prof" placeholder="Программист" required>
                            <div class="file-upload">
                                <label for="photo">Выбрать фотографию
                                    <input type="file" name="photo" id="photo">
                                    <span>Загрузить фото</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="message">Текст отзыва</label>
                            <textarea name="message"  data-name ="Отзыв" class="for-ajax-feedback" id="message"></textarea>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="submit">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->