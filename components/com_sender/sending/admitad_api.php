<?php

// No direct access
defined('_JEXEC') or die;
//todo вынести работу с результатами адмитад в отдельный класс
/**
 * Model for edit/create current element
 * @author Sapaleks
 */
class SendingModelAdmitadApi extends JModelList
{

	/**
	 * @var string
	 */
	private $client_id = '57ecb9ebd0d945c61e7a74f593b7dd';
	/**
	 * @var string
	 */
	private $client_secret = '1d26a6f281022921c1f976e2d2a002';
	/**
	 * @var string
	 */
	private $b64_encoded = 'NTdlY2I5ZWJkMGQ5NDVjNjFlN2E3NGY1OTNiN2RkOjFkMjZhNmYyODEwMjI5MjFjMWY5NzZlMmQyYTAwMg==';
	/**
	 * @var
	 */
	private $refresh_token;

	/**
	 * @var
	 */
	protected $site_id;
	/**
	 * @var
	 */
	protected $token;
	/**
	 * @var
	 */
	protected $campaigns;
	/**
	 * @bool
	 */
	protected $script = false;

	/**
	 *
	 */
	const campaign_ids = array(
		17398 => 'Кредит Днепр',
		14605 => 'Швидко Гроші',
		17288 => 'Cashpoint',
		18990 => 'Monobank',
		16243 => 'ПУМБ',
		16126 => 'PrivatBank',
		19417 => 'Unex',
		19059 => 'Ecredit',
		11701 => 'Ideabank',
		17540 => 'MILOAN',
		21483 => 'RwSbank ',
	);


	/**
	 * @return mixed
	 */
	public function getToken()
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->setQuery('SELECT token, refresh_token FROM #__tokens WHERE api_name = "admitad"');
		$db->setQuery($query);
		$result = $db->loadAssoc();
		$this->token = $result["token"];
		$this->refresh_token = $result["refresh_token"];
		return $result["token"];
	}

	/**
	 * @param $token
	 * @param $refresh_token
	 * @return bool
	 */
	public function updateToken($token, $refresh_token)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->setQuery('UPDATE #__tokens SET token = "' . $token . '", refresh_token = "' . $refresh_token . '" WHERE api_name = "admitad"');
		$db->setQuery($query);
		$db->execute();

		$this->token = $token;
		$this->refresh_token = $refresh_token;

		return true;
	}

	/**
	 * @return mixed
	 */
	public function generateToken()
	{
		$data = 'grant_type=client_credentials&client_id=' . $this->client_id . '&scope=advcampaigns_for_website arecords broker_application manage_advcampaigns websites advcampaigns manage_broker_application';
		$process = curl_init('https://api.admitad.com/token/');
		//curl_setopt($process, CURLOPT_HTTPHEADER, array('application/x-www-form-urlencoded;charset=UTF-8'));
		//curl_setopt($process, CURLOPT_HEADER, 1);
		curl_setopt($process, CURLOPT_USERPWD, $this->client_id . ":" . $this->client_secret);
		curl_setopt($process, CURLOPT_TIMEOUT, 30);
		curl_setopt($process, CURLOPT_POST, 1);
		curl_setopt($process, CURLOPT_POSTFIELDS, $data);
		curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
		$return = curl_exec($process);
		$return = json_decode($return, 1);
		curl_close($process);
		return $return;
	}

	/**
	 *
	 */
	public function generateB64Auth()
	{

	}

	/**
	 * @return mixed
	 */
	public function refreshToken()
	{
		$data = 'grant_type=refresh_token&client_id=' . $this->client_id . '&refresh_token=' . $this->refresh_token . '&client_secret=' . $this->client_secret;
		$process = curl_init('https://api.admitad.com/token/');
		curl_setopt($process, CURLOPT_HTTPHEADER, array('application/x-www-form-urlencoded;charset=UTF-8'));
		curl_setopt($process, CURLOPT_TIMEOUT, 30);
		curl_setopt($process, CURLOPT_POST, 1);
		curl_setopt($process, CURLOPT_POSTFIELDS, $data);
		curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
		$return = curl_exec($process);
		curl_close($process);
		$return = json_decode($return, 1);
		if ($return["access_token"] && $return["refresh_token"]) {
			$this->updateToken($return["access_token"], $return["refresh_token"]);
		} else {
			$this->ResultWriter($return);
			$res = $this->generateToken();
			$this->updateToken($res["access_token"], $res["refresh_token"]);
		}
		return $return;
	}


	/**
	 * @param $url
	 * @return mixed
	 */
	public function sendGetRequest($url)
	{
		$authorization = "Authorization: Bearer " . $this->token;
		$process = curl_init($url);
		curl_setopt($process, CURLOPT_HTTPHEADER, array('application/x-www-form-urlencoded;charset=UTF-8', $authorization));
		curl_setopt($process, CURLOPT_TIMEOUT, 30);
		curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
		$return = curl_exec($process);
		curl_close($process);
		$return = json_decode($return, 1);
		return $return;
	}

	/**
	 * @param $url
	 * @param $post
	 * @return mixed
	 */
	public function sendPostRequest($url, $post)
	{
		$authorization = "Authorization: Bearer " . $this->token;
		$process = curl_init($url);
		curl_setopt($process, CURLOPT_HTTPHEADER, array('application/x-www-form-urlencoded;charset=UTF-8', $authorization));
		//curl_setopt($process, CURLOPT_HEADER, 1);
		curl_setopt($process, CURLOPT_TIMEOUT, 30);
		curl_setopt($process, CURLOPT_POST, 1);
		curl_setopt($process, CURLOPT_POSTFIELDS, $post);
		curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
		$return = curl_exec($process);
		curl_close($process);
		$return = json_decode($return, 1);
		return $return;
	}

	/**
	 * @return mixed
	 */
	public function getAdmitadCampaigns()
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->setQuery('SELECT * FROM #__admitad_campaigns WHERE active = 1');
		$db->setQuery($query);
		$result = $db->loadAssocList('campaign_id');
		return $result;
	}

	/**
	 *
	 */
	public function updateCampaignsInfo()
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("UPDATE `#__admitad_campaigns` SET curl_count = 0, ad_count = 0, day_limit = 0");
		$db->setQuery($query);
		$db->execute();
	}

	/**
	 * @param $data
	 */
	public function resultWriter($data, $info = array())
	{
		$text = '';
		if (isset($data['errors'])) {
			foreach ($data['errors'] as $error) {
				$text = $text . (isset($error['campaign_id']) ? $this->campaigns[$error['campaign_id']]['name'] . ': ' : '');
				if (is_array($error['message'])) {
					foreach ($error['message'] as $message) {
						$text = $text . $message[0] . ': ' . $message[1][0] . ' ';
					}
				}
				$text = $text . "  !!!  ";
			}
			$this->to_log($text);
		} elseif (isset($data['error'])) {
			$text = $data['error'] . ': ' . $data['error_description'] . "  !!!  ";
			$this->to_log($text);
		} else {
			$this->to_log('Заявка отправлена успешно');
			$this->dbSaveRequest($data, $info);
		}
	}

	/**
	 * @param $data
	 */
	public function dbResultWriter($admitad_id, $data)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$keys = array_keys($data);
		foreach ($keys as $key) {
			$parts[] = $key . '= "' . addslashes($data[$key]) . '"';
		}
		$query->setQuery("UPDATE `#__admitad_results` SET " . implode($parts, ', ') . " WHERE admitad_id = " . $admitad_id);
		$db->setQuery($query);
		$db->execute();
	}

	/**
	 * @param $data
	 * @param $info
	 */
	public function dbSaveRequest($data, $info)
	{
		if ($data['id']) {
			$parts = array(
				$info['campaign_id'],
				$info['request_id'],
				$data['id'],
				0,
				(isset($data['responses']['status'])) ? $data['responses']['status'] : 'error',
			);
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			$query->setQuery('INSERT INTO `#__admitad_results` (campaign_id, request_id, admitad_id, result, result_text)  VALUES ("' . implode($parts, '", "') . '")');
			$db->setQuery($query);
			$db->execute();
		}

	}

	/**
	 * @return mixed
	 */
	public function getNewAdmitadRequests()
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$r = 'SELECT * FROM `#__admitad_results` WHERE result = 0';
		$query->setQuery($r);
		$db->setQuery($query);
		$data = $db->loadAssocList();

		return $data;
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	public function getRequestResult($id)
	{
		$url = 'https://api.admitad.com/website/' . $this->site_id . '/broker/applications/?id=' . $id;
		$result = $this->sendGetRequest($url);
		return $result;
	}


	/**
	 * @param $result
	 * @return bool
	 */
	public function checkError($result)
	{
		if (isset($result['error_code'])) {
			switch ($result['error_code']) {
				case 0:
					if ($this->refreshToken()) {
						//запись в лог
						$this->to_log('Обновлен токен для admitad');
					}

					break;
				case 1:
					$this->to_log('Неправильный или недействительный ключ');
					break;
			}
			return true;
		}
		return false;

	}


	/**
	 *
	 */
	public function checkToken()
	{
		$resp = $this->checkRules(17398);
		if ($this->checkError($resp)) {

		}
	}


	/**
	 * @param $id
	 * @return mixed
	 */
	public function checkRules($id)
	{
		$authorization = "Authorization: Bearer " . $this->token;

		$process = curl_init('https://api.admitad.com/broker/campaign_settings/' . $id . '/');
		curl_setopt($process, CURLOPT_HTTPHEADER, array('application/x-www-form-urlencoded;charset=UTF-8', $authorization));
		curl_setopt($process, CURLOPT_TIMEOUT, 30);
		curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
		$return = curl_exec($process);
		curl_close($process);
		$return = json_decode($return, 1);
		return $return;
	}

	/**
	 * @param $params
	 * @return mixed
	 */
	public function getOrderList($params)
	{
		$url = 'https://api.admitad.com/website/' . $this->site_id . '/broker/applications/?' . http_build_query($params);
		$result = $this->sendGetRequest($url);
		return $result;
	}

	/**
	 * @param $id
	 * @param $respond
	 * @param bool $limit
	 * @return bool
	 */
	public function checkLimit($id, $respond, $limit = false)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		if (isset($respond['errors']) && !$limit) {
			$text = '';
			foreach ($respond['errors'] as $error) {
				$text = $text . (isset($error['campaign_id']) ? self::campaign_ids[$error['campaign_id']] . ': ' : '');
				if (is_array($error['message'])) {
					foreach ($error['message'] as $message) {
						$text = $text . $message[0] . ': ' . $message[1][0] . ' ';
					}
				}
			}


			if (stristr($text, 'Вы превысили ограничение по количеству заявок')) {
				$query->setQuery('UPDATE #__admitad_campaigns SET day_limit = 1 WHERE campaign_id = ' . $id);
				$db->setQuery($query);
				$db->execute();
			}
		} elseif ($limit && !isset($respond['errors'])) {
			$query->setQuery('UPDATE #__admitad_campaigns SET day_limit = 0 WHERE campaign_id = ' . $id);
			$db->setQuery($query);
			$db->execute();
		}

		return true;
	}

	/**
	 * @param $id
	 * @param $count
	 * @return bool
	 */
	public function incrementAd($id, $count)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->setQuery('UPDATE #__admitad_campaigns SET ad_count = ' . ($count + 1) . ' WHERE campaign_id = ' . $id);
		$db->setQuery($query);
		$db->execute();

		return true;
	}

	/**
	 * @param $id
	 * @param $count
	 * @return bool
	 */
	public function incrementCurl($id, $count)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->setQuery('UPDATE #__admitad_campaigns SET curl_count = ' . ($count + 1) . ' WHERE campaign_id = ' . $id);
		$db->setQuery($query);
		$db->execute();

		return true;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function sendRequest($data)
	{
		$data['campaigns'] = json_encode($data['campaigns']);
		$resp = $this->sendPostRequest('https://api.admitad.com/website/' . $this->site_id . '/broker/applications/create/', $data);
		return $resp;
	}

	/**
	 * @param $str
	 * @return bool
	 */
	public function to_log($str)
	{
		if($this->script) {
			JLog::add($str, \JLog::DEBUG, 'script_sender');
		}else {
			JLog::add($str, \JLog::DEBUG, 'com_sender');
		}

		return false;
	}



}