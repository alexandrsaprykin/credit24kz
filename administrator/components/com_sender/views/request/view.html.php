<?php

// No direct access
defined( '_JEXEC' ) or die;

/**
 * View for edit current element
 * @author Sapaleks
 */
class SenderViewRequest extends JViewLegacy
{

	/**
	 * @var $form JForm
	 */
	public $form;
	/**
	 * @var $item stdClass
	 */
	public $item;	/**
	 * @var $item stdClass
	 */
	public $marks;
	/**
	 * @var $user JUser
	 */
	public $user;
	/**
	 * @var $tags stdClass[]
	 */
	public $state;
	/**
	 * @var $tags stdClass[]
	 */
	public $banks;
	/**
	 * @var $user JUser
	 */

	/**
	 * Method to display the current pattern
	 * @param String $tpl
	 */
	public function display( $tpl = null )
	{
		$this->form = $this->get( 'Form' );
		$this->item = $this->get( 'Item' );
		$this->admitad = $this->get('Admitad');
		$this->banks = $this->get('Banks');
		$this->marks = $this->get('Marks');
		$this->user = JFactory::getUser();
		$this->state = $this->get( 'State' );
		if ( count( $errors = $this->get( 'Errors' ) ) ) {
			JError::raiseError( 500, implode( '\n', $errors ) );
			return false;
		}
		$this->loadHelper( 'sender' );
		$this->canDo = senderHelper::getActions( 'request' );
		$this->_setToolBar();
		parent::display( $tpl );
	}

	/**
	 * Method to display the toolbar
	 */
	protected function _setToolBar()
	{
		JFactory::getApplication()->input->set( 'hidemainmenu', true );

		JToolBarHelper::title( 'Информация о заявке' );
		JToolbarHelper::apply('request.apply');
		JToolBarHelper::cancel( 'request.cancel', 'JTOOLBAR_CLOSE' );
		JToolBarHelper::custom('', 'redo', '124', 'Отправить через скрипт', false);
	}
}