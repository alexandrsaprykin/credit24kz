<?php
//sending (credit24.org.ua)
//ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

define("delay", 60); // задержка заявки перед отправкой (в секундах)

//// Граничная сумма, выше которой заявка не передается

define("aval_limit", 1000000);  // Аваль
define("pumb_limit", 1000000); //  ПУМБ
define("dnepr_limit", 1000000); //  Кредит-Днепр
define("kredo_limit", 1000000); //  Кредо


// Initialize Joomla framework
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
	require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
	define('JPATH_BASE', dirname(__FILE__));
	require_once JPATH_BASE . '/includes/defines.php';
}
define('JPATH_COMPONENT', JPATH_BASE . '/components/com_sender');

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

// Load the configuration
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_BASE . '/includes/framework.php';

require_once JPATH_COMPONENT . '/helpers/sender.php';

$mainframe = JFactory::getApplication('site');
$mainframe->initialise();

$db = JFactory::getDbo();

jimport('joomla.application.component.helper');

JModelLegacy::addIncludePath(JPATH_COMPONENT . '/sending', 'SendingModel');

senderSiteHelper::initLogger();

$r_model = JModelLegacy::getInstance('Request', 'SendingModel');
$mail_model = JModelLegacy::getInstance('Mail', 'SendingModel');
$ad_model = JModelLegacy::getInstance('Admitad', 'SendingModel');
$cond_model = JModelLegacy::getInstance('Admitad', 'SendingModel');


function to_log($str)
{
	JLog::add($str, \JLog::DEBUG, 'com_sender');

	return false;
}

$site_name = JFactory::getConfig()->get('sitename');
$com_sender = JComponentHelper::getComponent('com_sender');
$params = new JRegistry($com_sender->getParams());
$ad_id = $params->get('admitad_id');
$developer_mode = $params->get('developer_mode');
$idea_form_id = $params->get('idea_form_id');
$finline_chanel = $params->get('finline_chanel');
$cash2go_login = $params->get('cash2go_login');
$cash2go_pass = $params->get('cash2go_pass');
$mailer = JFactory::getMailer();
$mailer->isHTML(true);

//отправка заявок
$all_data = $r_model->getNotSended(delay);
foreach ($all_data as $key => $data) {//назначим заявкам статус "отправляется"
	$data2 = array(
		'sended' => 2,
	);

	$r_model->update($data2, $data['id']);
}

foreach ($all_data as $key => $data) {


	to_log('----------------------------');
	to_log('Обработка заявки от ' . $data['name'] . ' (' . $data['phone'] . ') (' . $data['email'] . ') ИНН: ' . $data['inn']);

	if (!$data['confirm_result_log'] and $data['confirm_result']) {
		to_log($data['confirm_result']);
		$data2 = array(
			'confirm_result_log' => 1,
		);
		$r_model->update($data2, $data['id']);
	}

	//нереальный возраст
	$data['age2'] = 0;
	if ($data['age'] > 100 || $data['age'] < 15) {
		$data['age2'] = $data['age'];
		$data['age'] = 40;
	}

	$mailer->setSubject('Заявка ' . $site_name);
	$number = substr($data['inn'], 0, 5);
	$b_date = date('d.m.Y', strtotime('01.01.1900 + ' . $number . ' days - 1 days'));


	$msg1 = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>
	<br><b>ФИО:</b> " . $data['name'] . "<br>
	<b>Телефон:</b> " . $data['phone'] . "<br>
	<b>E-mail:</b> " . $data['email'] . "<br>
	<b>Идентификационный код:</b> " . $data['inn'] . "<br>
	<b>Дата рождения:</b> " . $b_date . "<br>
	<b>Возраст:</b> " . $data['age'] . "<br>
	<b>Тип дохода:</b> " . $data['tip'] . "<br>
	<b>Сумма кредита:</b> " . $data['sum'] . " грн.<br>
	<b>Ссылка:</b> <a href=\"{{URL}}\">{{URL}}</a><br>
	</body></html>";

	$msg2 = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>
	<br><b>ФИО:</b> " . $data['name'] . "<br>
	<b>Телефон:</b> " . $data['phone'] . "<br>
	<b>E-mail:</b> " . $data['email'] . "<br>
	<b>Идентификационный код:</b> " . $data['inn'] . "<br>
	<b>Дата рождения:</b> " . $b_date . "<br>
	<b>Возраст:</b> " . $data['age'] . "<br>
	<b>Тип дохода:</b> " . $data['tip'] . "<br>
	<b>Сумма кредита:</b> " . $data['sum'] . " грн.<br>
	</body></html>";


	//основное письмо
	$phone = substr_replace("+38(" . $data['phone'], ")", 7, 0);
	$phone2 = $data['phone2'] ? substr_replace("+38(" . $data['phone2'], ")", 7, 0) : '';
	$msg = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>
	<br><b>ФИО:</b> " . $data['name'] . "<br>
	<b>Телефон:</b> " . $phone . "<br>
	<b>Идентификационный код:</b> " . $data['inn'] . "<br>
	<b>Возраст:</b> " . $data['age'] . "<br>
	<b>Область фактического проживания:</b> " . $data['obl'] . "<br>
	<b>Тип дохода:</b> " . $data['tip'] . "<br>
	<b>Сумма кредита:</b> " . $data['sum'] . " грн.<br>
	<b>Результат заявки Адмитад-Альфа:</b> " . $data['alfa_result'] . "<br>
	<b>Источник:</b> " . $data['referer'] . "<br>
	<b>Источник заявки:</b> " . $data['istok'] . "<br>
	<b>Девайс:</b> " . $data['device'] . "<br>
	<b>E-mail:</b> " . $data['email'] . "<br>
	</body></html>";

	$mail_model->start(false, $site_name, $developer_mode);
	$mail_model->sending_alfa($data, $mailer, $msg1);
	$mail_model->sending_kazkreditline($data, $mailer, $msg1);
	$mail_model->sending_homekredit($data, $mailer, $msg1);
	$mail_model->sending_allkz($data, $mailer, $msg);


	if ($data['log']) {
		to_log($data['log']);
	}
	to_log('конец обработки заявки от ' . $data['name']);

	$data2 = array(
		'sended' => 1,
	);

	$r_model->update($data2, $data['id']);

}

