<?php

// No direct access
defined('_JEXEC') or die;

/**
 * @author Sapaleks
 */
class SenderModelStatistics extends JModelList
{

	/**
	 * Конструктор класса
	 * @param Array $config
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array('id');
		}
		parent::__construct($config);
	}

	/**
	 * @param String $ordering
	 * @param String $direction
	 */
	protected function populateState($ordering = 'id', $direction = 'desc')
	{
		if ($layout = JFactory::getApplication()->input->get('layout')) {
			$this->context .= '.' . $layout;
		}
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		parent::populateState($ordering, $direction);
	}

	/**
	 * @param string $id
	 * @return string
	 */
	protected function getStoreId($id = '')
	{
		$id .= ':' . $this->getState('filter.search');

		return parent::getStoreId($id);
	}

	/**
	 * Overrides the getItems method to attach additional metrics to the list.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */
	public function getItems()
	{
		// Get a storage key.
		$store = $this->getStoreId('getItems');

		// Try to load the data from internal storage.
		if (!empty($this->cache[$store]))
		{
			return $this->cache[$store];
		}

		// Load the list items.
		$items = parent::getItems();

		// If emtpy or an error, just return.
		if (empty($items))
		{
			return array();
		}

		$db = $this->getDbo();

		$day = strtotime(date('Y-m-d'));
		$month = strtotime(date('Y-m-01'));

		foreach ($items as $item) {

			$query = $db->getQuery(true)
				->select('COUNT(*)')
				->from('#__bank_requests AS br')
				->where('br.worker_id = ' . $item->id)
				->where('br.sending_time > ' . $day);

			$db->setQuery($query);

			try {
				$item->day = $db->loadRow()[0];
			} catch (RuntimeException $e) {
				$this->setError($e->getMessage());
				return false;
			}


			$query = $db->getQuery(true)
				->select('COUNT(*)')
				->from('#__bank_requests AS br')
				->where('br.worker_id = ' . $item->id)
				->where('br.sending_time > ' . $month);

			$db->setQuery($query);

			try {
				$item->month = $db->loadRow()[0];
			} catch (RuntimeException $e) {
				$this->setError($e->getMessage());
				return false;
			}

		}

		// Getting the following metric by joins is WAY TOO SLOW.
		// Faster to do three queries for very large menu trees.


		// Add the items to the internal cache.
		$this->cache[$store] = $items;

		return $this->cache[$store];
	}

	/**
	 * Составление запроса для получения списка записей
	 * @return JDatabaseQuery
	 */
	protected function getListQuery()
	{
		$query = $this->getDbo()->getQuery(true);
		$query->select('u.*');
		//$query->select( 'u.username as created_by, u.id as author_id' );
		//$query->join( 'LEFT', '#__admitad AS ad ON u.id = t1.created_by' );
		$query->from('#__users as u');
		/*$published = $this->getState( 'filter.published' );
		if ( is_numeric( $published ) ) {
			$query->where( 't1.published=' . (int)$published );
		}
		$authorId = $this->getState( 'filter.author_id' );
		if ( is_numeric( $authorId ) ) {
			$query->where( 'u.id=' . $authorId );
		}*/
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $this->getDbo()->Quote('%' . $this->getDbo()->escape($search, true) . '%');
			$query->where('(u.name LIKE ' . $search . ' OR u.username LIKE '  . $search . ')');
		}
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');
		$query->order($this->getDbo()->escape($orderCol . ' ' . $orderDirn));
		return $query;
	}

	/**
	 * Авторы записей
	 * @return    JDatabaseQuery
	 */
	public function getAuthors()
	{
		$query = $this->getDbo()->getQuery(true);
		$query->select('u.id AS value, u.name AS text');
		$query->from('#__users AS u');
		$query->join('INNER', '#__requests AS c ON c.created_by = u.id');
		$query->group('u.id, u.name');
		$query->order('u.name');
		return $this->getDbo()->setQuery($query)->loadObjectList();
	}


}